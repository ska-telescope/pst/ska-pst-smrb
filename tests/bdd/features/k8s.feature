Feature: SMRB Component
Scenario Outline: Deploy pod with a valid configuration
    Given a <manifest> contains valid k8s configuration
    And label <k8slabel> is defined in <manifest>
    When k8s manifest is scheduled for deployment
    When k8s pods are created
    When k8s configmaps are created
    When k8s services are created
    Then k8s pods exist in the namespace
    Then k8s configmaps exist in the namespace
    Then k8s services exist in the namespace

    Examples:
    | manifest | k8slabel |
    | k8s-tc-1-valid.yaml | app.kubernetes.io/name=ska-pst-smrb |


Scenario Outline: reserve SMRB resources inside a container
    Given a <manifest> contains valid k8s configuration
    And label <k8slabel> is defined in <manifest>
    When k8s pods are created
    When k8s configmaps are created
    When k8s services are created
    When k8s pods with label <k8slabel> are running
    Then pod logs confirm that smrb resources are reserved

    Examples:
    | manifest | k8slabel |
    | k8s-tc-1-valid.yaml | app.kubernetes.io/name=ska-pst-smrb |


Scenario Outline: delete SMRB resources inside a container
    Given label <k8slabel> is defined in <manifest>
    When k8s pods are created
    When k8s configmaps are created
    When k8s services are created
    When k8s pods with label <k8slabel> are running
    When k8s <manifest> resources with <k8slabel> are scheduled for deletion
    Then k8s resources were deleted

    Examples:
    | manifest | k8slabel |
    | k8s-tc-1-valid.yaml | app.kubernetes.io/name=ska-pst-smrb |
