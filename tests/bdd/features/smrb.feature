Feature: SMRB Component
Scenario: Invalid configuration
    Given ska_pst_smrb_core uses a configuration file
    And The configuration file(s) is/are invalid
    When ska_pst_smrb_core executes with the invalid configuration file(s)
    Then ska_pst_smrb_core with invalid configuration terminates gracefully with an exception

Scenario: SIGINT received
    Given ska_pst_smrb_core uses a configuration file
    When signal is SIGINT
    When The configuration file is valid
    When ska_pst_smrb_core executes with the valid configuration file(s) and receives signal
    Then ska_pst_smrb_core with valid configuration receiving SIGINT terminates gracefully with an exception

Scenario: SIGTERM received
    Given ska_pst_smrb_core uses a configuration file
    When signal is SIGTERM
    When The configuration file is valid
    When ska_pst_smrb_core executes with the valid configuration file(s) and receives signal
    Then ska_pst_smrb_core with valid configuration receiving SIGTERM terminates gracefully with an exception
    