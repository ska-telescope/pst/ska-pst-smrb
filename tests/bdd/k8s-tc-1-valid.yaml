# Source: ska-pst-smrb/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: ska-pst-smrb-configs
  labels:
    helm.sh/chart: ska-pst-smrb-0.10.6
    app: ska-pst-smrb
    app.kubernetes.io/name: ska-pst-smrb
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "0.0.0"
    app.kubernetes.io/managed-by: Helm
data:
  config.txt: |-
    # Placeholder
    DATA_KEY        a020
    WEIGHTS_KEY     a030
    NUMA_NODE       0
    # common header ring buffer defintion
    HB_NBUFS        8
    HB_BUFSZ        4096
    # data ring buffer defintion [180 packets for MidPSTBand5]
    DB_NBUFS        8
    DB_BUFSZ        33566400
    # weights ring buffer defintion [180 packets for MidPSTBand5]
    WB_NBUFS        8
    WB_BUFSZ        4195800
  data_header.txt: |-
    # Placeholder
    HDR_SIZE            4096
    HDR_VERSION         1.0
    # Parameters for MidPSTBand5 sub-band 1
    NCHAN               11655
    NBIT                8
    NPOL                2
    NDIM                2
    TSAMP               16.276041667
    FREQ                4913.2864
    BW                  626.5728
    START_CHANNEL       0
    END_CHANNEL         11655
    BYTES_PER_SECOND    2864332800.000
    RESOLUTION          186480
    # Additional parameters, not required for this test
    DATA_HOST           127.0.0.1
    DATA_PORT           12345
    CALFREQ             11.1111111111
    OBS_OFFSET          0
    UTC_START           2022-01-01-00:00:00
  tc-1-invalid-value.txt: |-
    # Placeholder
    DATA_KEY        @!00
    WEIGHTS_KEY     @!01
    NUMA_NODE       0
    # common header ring buffer defintion
    HB_NBUFS        8
    HB_BUFSZ        4096
    # data ring buffer defintion [180 packets for MidPSTBand5]
    DB_NBUFS        8
    DB_BUFSZ        33566400
    # weights ring buffer defintion [180 packets for MidPSTBand5]
    WB_NBUFS        8
    WB_BUFSZ        4195800
  tc-1-missing-keys.txt: |-
    # Placeholder
    # DATA_KEY        a000
    # WEIGHTS_KEY     a010
    NUMA_NODE       0
    # common header ring buffer defintion
    HB_NBUFS        8
    HB_BUFSZ        4096
    # data ring buffer defintion [180 packets for MidPSTBand5]
    DB_NBUFS        8
    DB_BUFSZ        33566400
    # weights ring buffer defintion [180 packets for MidPSTBand5]
    WB_NBUFS        8
    WB_BUFSZ        4195800
  weights_header.txt: |-
    # Placeholder
    HDR_SIZE            4096
    HDR_VERSION         1.0
    # Parameters for MidPSTBand5 sub-band 1
    NCHAN               11655
    NBIT                16
    NPOL                1
    NDIM                1
    TSAMP               65.104166668
    FREQ                4913.2864
    BW                  626.5728
    START_CHANNEL       0
    END_CHANNEL         11655
    BYTES_PER_SECOND    358041599.9926673
    RESOLUTION          23310
    # Additional parameters, not required for this test
    DATA_HOST           127.0.0.1
    DATA_PORT           12345
    CALFREQ             11.1111111111
    OBS_OFFSET          0
    UTC_START           2022-01-01-00:00:00
---
# Source: ska-pst-smrb/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: smrb
  labels:
    helm.sh/chart: ska-pst-smrb-0.10.6
    app: ska-pst-smrb
    app.kubernetes.io/name: ska-pst-smrb
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "0.0.0"
    app.kubernetes.io/managed-by: Helm
data:
  SMRB_CONFIG_FILE: config.txt
  SMRB_CONFIG_PATH: /mnt/configurations
  SMRB_CORE_CMD: /usr/local/bin/ska_pst_smrb_core
  SMRB_DATA_HEADER_FILE: data_header.txt
  SMRB_READER_CMD: /usr/local/bin/ska_pst_smrb_reader
  SMRB_WEIGHTS_HEADER_FILE: weights_header.txt
  SMRB_WRITER_CMD: /usr/local/bin/ska_pst_smrb_writer
---
# Source: ska-pst-smrb/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: test-ska-pst-smrb
  labels:
    helm.sh/chart: ska-pst-smrb-0.10.6
    app: ska-pst-smrb
    app.kubernetes.io/name: ska-pst-smrb
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "0.0.0"
    app.kubernetes.io/managed-by: Helm
spec:
  type: ClusterIP
  ports:
    - port: 8080
      targetPort: 8080
      protocol: TCP
      name: http
  selector:
    app: ska-pst-smrb
    app.kubernetes.io/name: ska-pst-smrb
    app.kubernetes.io/instance: test
---
# Source: ska-pst-smrb/templates/pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-ska-pst-smrb
  labels:
    helm.sh/chart: ska-pst-smrb-0.10.6
    app: ska-pst-smrb
    app.kubernetes.io/name: ska-pst-smrb
    app.kubernetes.io/instance: test
    app.kubernetes.io/version: "0.0.0"
    app.kubernetes.io/managed-by: Helm
spec:
  hostNetwork: false
  initContainers: []
  containers:
    - name: ska-pst-smrb
      securityContext:
            {}
      image: "registry.gitlab.com/ska-telescope/pst/ska-pst-smrb/ska-pst-smrb:0.10.6"
      command: ["$(SMRB_CORE_CMD)"]
      args:
        - "-v"
        - "-f"
        - "$(SMRB_CONFIG_PATH)/$(SMRB_CONFIG_FILE)"
      envFrom:
        - configMapRef:
            name: smrb
      volumeMounts:
        - mountPath: /mnt/configurations
          name: ska-pst-smrb-configs
      ports:
        - containerPort: 8080
  volumes:
      - name: ska-pst-smrb-configs
        configMap:
          name: ska-pst-smrb-configs
