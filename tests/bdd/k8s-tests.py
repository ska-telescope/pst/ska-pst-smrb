from kubernetes import client, config, utils, watch

# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()
v1 = client.CoreV1Api()
k8s_client = client.ApiClient()

import pytest
from pytest_bdd import given, when, scenarios, then
from pytest_bdd.parsers import parse

from time import sleep
import os

scenarios('k8s.feature')

def test_created_resource(requested_resource_list=[], existing_resource_list=[]):
    assert all(requested_resource_name in existing_resource_list for requested_resource_name in requested_resource_list)

@pytest.fixture
def k8snamespace():
    return os.environ.get("KUBE_NAMESPACE")


@pytest.fixture
def results():
    return {}

@pytest.fixture
def k8s_action():
    return ""

@pytest.fixture
def k8sconfig():
    return ""

@pytest.fixture
def k8slabel():
    return ""
# def config():
#     return ""

@pytest.fixture
def namespace_pods():
    return {}

@pytest.fixture
def namespace_pods_with_label():
    return {}

@pytest.fixture
def namespace_config_maps():
    return {}

@pytest.fixture
def namespace_services():
    return {}

@given(parse('a {manifest} contains valid k8s configuration'), target_fixture="k8sconfig")
def configs_is_valid(manifest):
    return str(manifest)

@given(parse('label {k8slabel} is defined in {manifest}'), target_fixture="k8slabel")
def k8slabel_is_present(k8slabel):
    return str(k8slabel)
# @given("The target k8s manifest is valid", target_fixture="configs")
# def configs_is_valid():
#     return ["k8s-tc-1-valid.yaml"]

@when("k8s manifest is scheduled for deployment")
def k8s_deploy_from_yaml(k8snamespace, k8slabel, k8sconfig, results):
    resource_list = {}
    resources_raw = utils.create_from_yaml(k8s_client,k8sconfig,namespace=k8snamespace,verbose=True)

    podstate=""
    desiredstate="Running"
    attempts=0
    while desiredstate not in podstate:
        try:
            print(f"k8s_deploy_from_yaml::attempts:{attempts}")
            pod_list = v1.list_namespaced_pod(
                namespace=k8snamespace, 
                label_selector=k8slabel
            )
            for pod_details in pod_list.items:
                podstate = pod_details.status.phase
                attempts += 1
                assert desiredstate in podstate
        except Exception as e:
            sleep(1)
            print(f"k8s_deploy_from_yaml::podstate:e:{e}")
    

    attempts=0
    while resources_raw is None:
        try:
            attempts += 1
            print(f"k8s_deploy_from_yaml::resources_raw:attempts: {attempts}")
            resources_raw = utils.create_from_yaml(k8s_client,k8sconfig,namespace=k8snamespace,verbose=True)
        except Exception as e:
            assert resources_raw is not None
            print(f"e: {e}")
            sleep(1)

    for resource in resources_raw:
        if resource[0].kind not in resource_list.keys():
            resource_list[resource[0].kind] = []

        resource_list[resource[0].kind].append(
            resource[0].metadata.name
        )

    results[f"deploy-{k8sconfig}"] = resource_list


@when("k8s pods are created", target_fixture="namespace_pods")
def then_k8s_pods_created(k8snamespace):
    raw_all_pods = v1.list_namespaced_pod(namespace=k8snamespace)
    return raw_all_pods


@when("k8s configmaps are created", target_fixture="namespace_config_maps")
def then_k8s_config_maps_created(k8snamespace):
    raw_all_config_map = v1.list_namespaced_config_map(namespace=k8snamespace)
    return raw_all_config_map


@when("k8s services are created", target_fixture="namespace_services")
def then_k8s_services_created(k8snamespace):
    raw_all_service = v1.list_namespaced_service(namespace=k8snamespace)
    return raw_all_service


@then("k8s pods exist in the namespace")
def then_k8s_pods_created(k8sconfig, results, namespace_pods):
    resource_list = results[f"deploy-{k8sconfig}"]["Pod"]
    pod_list = [pod_details.metadata.name for pod_details in namespace_pods.items]
    test_created_resource(resource_list, pod_list)

@then("k8s configmaps exist in the namespace")
def then_k8s_config_maps_created(k8sconfig, results, namespace_config_maps):
    resource_list = results[f"deploy-{k8sconfig}"]['ConfigMap']
    config_map_list = [config_map_details.metadata.name for config_map_details in namespace_config_maps.items]
    test_created_resource(resource_list, config_map_list)


@then("k8s services exist in the namespace")
def then_k8s_services_created(k8sconfig, results, namespace_services):
    resource_list = results[f"deploy-{k8sconfig}"]['Service']
    service_list = [service_details.metadata.name for service_details in namespace_services.items]
    test_created_resource(resource_list, service_list)


@when(parse("k8s pods with label {k8slabel} are running"))
def when_pods_running(k8slabel, k8snamespace, namespace_pods):
    attempts=0
    podstate=""
    desiredstate="Running"
    while desiredstate not in podstate:
        try:
            print(f"when_pods_running::attempts:{attempts}")
            pod_list = v1.list_namespaced_pod(
                namespace=k8snamespace, 
                label_selector=k8slabel
            )
            for pod_details in pod_list.items:
                podstate = pod_details.status.phase
                attempts += 1
                assert desiredstate in podstate
        except Exception as e:
            sleep(1)
            print(f"when_pods_running::podstate:e:{e}")
        else:
            assert desiredstate in podstate


@then("pod logs confirm that smrb resources are reserved")
def smrb_resources_reserved(namespace_pods):
    for pod_details in namespace_pods.items:
        log = v1.read_namespaced_pod_log(
                namespace=pod_details.metadata.namespace, 
                name=pod_details.metadata.name
            )
        assert "Initializing data and weights stats" in log
        assert "Creating data and weights ring buffers" in log
        assert "waiting for termination condition" in log


@when(parse('k8s {manifest} resources with {k8slabel} are scheduled for deletion'), target_fixture="results")
def schedule_delete(k8slabel, results, namespace_pods, namespace_config_maps, namespace_services):
    attempts=0
    current_resources = {}
    current_pod_count=1
    while current_pod_count != 0:
        print(f"schedule_delete::current_pod_count::attempts:{attempts}")
        try:
            for cached_pod in namespace_pods.items:
                current_pod_list = v1.list_namespaced_pod(
                    namespace=cached_pod.metadata.namespace, 
                    label_selector=k8slabel
                )

                for pod in current_pod_list.items:
                    pod_delete = v1.delete_namespaced_pod(
                        namespace=pod.metadata.namespace,
                        name=pod.metadata.name
                    )
                current_pod_count = len(current_pod_list.items)
                attempts += 1
                assert current_pod_count == 0
        except Exception as e:
            sleep(1)
            print(f"schedule_delete::pod:e:{e}")
        else:
            current_resources["Pod"] = current_pod_list.items

    attempts=0
    current_config_map_count=1
    while current_config_map_count != 0:
        print(f"schedule_delete::current_config_map_count::attempts:{attempts}")
        try:
            for cached_config_map in namespace_config_maps.items:
                current_config_map_list = v1.list_namespaced_config_map(
                    namespace=cached_config_map.metadata.namespace, 
                    label_selector=k8slabel
                )

                for config_map in current_config_map_list.items:
                    config_map_delete = v1.delete_namespaced_config_map(
                        namespace=config_map.metadata.namespace,
                        name=config_map.metadata.name
                    )
                current_config_map_count = len(current_config_map_list.items)
                attempts += 1
                assert current_config_map_count == 0
        except Exception as e:
            sleep(1)
            print(f"schedule_delete::config_map:e:{e}")
        else:
            current_resources["ConfigMap"] = current_config_map_list.items
    
    attempts=0
    current_service_count=1
    while current_service_count != 0:
        print(f"schedule_delete::current_service_count::attempts:{attempts}")
        try:
            for cached_service in namespace_services.items:
                current_services_list = v1.list_namespaced_service(
                    namespace=cached_service.metadata.namespace, 
                    label_selector=k8slabel
                )

                for service in current_services_list.items:
                    pod_delete = v1.delete_namespaced_service(
                        namespace=service.metadata.namespace,
                        name=service.metadata.name
                    )
                current_service_count = len(current_services_list.items)
                attempts += 1
                assert current_service_count == 0
        except Exception as e:
            sleep(1)
            print(f"schedule_delete::service:e:{e}")
        else:
            current_resources["Service"] = current_services_list.items
    
    return current_resources

@then(parse("k8s resources were deleted"))
def schedule_deleted(results):
    for resource in results:
        assert len(results[resource]) == 0