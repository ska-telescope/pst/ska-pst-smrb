ARG BUILD_IMAGE=""
FROM $BUILD_IMAGE as builder

ENV DEBIAN_FRONTEND=noninteractive
ENV LD_LIBRARY_PATH=/usr/local/lib/

ARG DEPENDENCIES_PATH=dependencies/ska-pst-smrb

# Copy installation payloads
COPY . /mnt/ska-pst-smrb
WORKDIR /mnt/ska-pst-smrb

RUN ls -la src

# Install Application
RUN make local-cpp-clean-buildpath \
    && make local-cpp-build-release \
    && cd build && make install \
    && /usr/local/bin/ska_pst_smrb_info \
    && /usr/local/bin/ska_pst_smrb_writer -h \
    && /usr/local/bin/ska_pst_smrb_reader -h \
    && /usr/local/bin/ska_pst_smrb_core -h

WORKDIR /usr/local/bin
CMD ["/bin/bash"]

FROM ubuntu:22.04 as runtime
ARG UID=1000
ARG GID=1000
ARG UNAME=pst

RUN groupadd -g $GID $UNAME  && \
    useradd -m -u $UID -g $GID -s /bin/bash $UNAME

WORKDIR /usr/local
COPY --from=builder /usr/local/bin/ska_pst_smrb* ./bin/
COPY --from=builder /usr/local/bin/dada_* ./bin/
COPY --from=builder /usr/local/lib/libpsrdada.so* ./lib/
COPY --from=builder /usr/local/lib/libska_pst_smrb.so* ./lib/

# Copy LMC/gRPC required libraries
COPY --from=builder /usr/local/lib/libska_pst_lmc.so* ./lib/
COPY --from=builder /usr/local/lib/libska_pst_common.so* ./lib/
COPY --from=builder /usr/local/lib/libprotobuf*.so* ./lib/
COPY --from=builder /usr/local/lib/libgrpc*.so* ./lib/
COPY --from=builder /usr/local/lib/libabsl*.so* ./lib/
COPY --from=builder /usr/local/lib/libaddress_sorting.so* ./lib/
COPY --from=builder /usr/local/lib/libcrypto.so* ./lib/
COPY --from=builder /usr/local/lib/libgpr.so* ./lib/
COPY --from=builder /usr/local/lib/libre2.so* ./lib/
COPY --from=builder /usr/local/lib/libssl.so* ./lib/
COPY --from=builder /usr/local/lib/libupb.so* ./lib/
COPY --from=builder /usr/local/lib/libz.so* ./lib/

COPY --from=builder /mnt/ska-pst-smrb/src/apps/data/*.txt ./share/

ENV DEBIAN_FRONTEND=noninteractive
ENV LD_LIBRARY_PATH=/usr/local/lib/

USER $UNAME

# RUNTIME TEST
RUN ska_pst_smrb_core -h \
    && ska_pst_smrb_info \
    && ska_pst_smrb_writer -h \
    && ska_pst_smrb_reader -h


WORKDIR /usr/local/bin
CMD ["/bin/bash"]