cmake_minimum_required(VERSION 3.5)

file(READ ".release" ver)
string(REGEX MATCH "release=([0-9]+.[0-9]+.[0-9]+)" _ ${ver})
set(SkaPstSmrb_VERSION ${CMAKE_MATCH_1})

message(STATUS "Building SkaPstSmrb version ${SkaPstSmrb_VERSION}")

# Project configuration, specifying version, languages,
# and the C++ standard to use for the whole project
set(PACKAGE_NAME SkaPstSmrb)
set(DESCRIPTION "Shared memory ring buffer for the SKA Pulsar Timing instrument")

project(${PACKAGE_NAME} VERSION ${SkaPstSmrb_VERSION} DESCRIPTION ${DESCRIPTION} LANGUAGES CXX )
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/resources/cmake-modules ${PROJECT_SOURCE_DIR}/cmake)
include(dependencies)
include(lint)
include(CTest)

# External projects
if (BUILD_TESTING)
  find_package(GTest REQUIRED)
endif()

find_package(psrdada REQUIRED)
find_package(Protobuf)
find_package(gRPC)
find_package(spdlog)
find_package(SkaPstLmc)
find_package(SkaPstCommon)

find_dependencies()
add_subdirectory(src/ska/pst/smrb)
add_subdirectory(src/apps)

# Install cmake config + version + target files
include(CMakePackageConfigHelpers)
configure_package_config_file(
  cmake/SkaPstSmrbConfig.cmake.in
  "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SkaPstSmrbConfig.cmake"
  INSTALL_DESTINATION
    cmake/ska_pst_smrb
)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SkaPstSmrbVersion.cmake"
  COMPATIBILITY
    AnyNewerVersion
)
install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SkaPstSmrbConfig.cmake
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/SkaPstSmrbVersion.cmake
  DESTINATION
    cmake/ska_pst_smrb
  COMPONENT
    dev
)

# Deb Packing
include(Packing)
