=================================
Application Programming Interface
=================================



The ska-pst-smrb library provides a Shared Memory Ring Buffer API that is used by the applications. This API is described below.


.. include:: class_view_hierarchy.rst.include

.. include:: file_view_hierarchy.rst.include

.. include:: unabridged_api.rst.include

