SKA-PST-SMRB
=============

This project provides the libraries and applications required for the Shared Memory Ring Buffer (SMRB) 
software component of the Square Kilometre Array (SKA)'s Pulsar Timing (PST) instrument. 

.. Architecture =================================================

.. toctree::
  :maxdepth: 2
  :caption: Architecture
  :hidden:

  architecture/index

.. Applications ============================================================

.. toctree::
  :maxdepth: 2
  :caption: Applications
  :hidden:

  apps/index

.. Development ============================================================

.. toctree::
  :caption: Development
  :maxdepth: 2

  Gitlab README<../../README.md>
  Libary API<api/library_root>
