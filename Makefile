## The following should be standard includes
# include core makefile targets for release management
include .make/base.mk

# include oci makefile targets for oci management
include .make/oci.mk

# include helm make support
include .make/helm.mk

# include k8s make support
include .make/k8s.mk

# TEST: common pst makefile library
include .pst/base.mk

DEV_IMAGE 	?= registry.gitlab.com/ska-telescope/pst/ska-pst-common/ska-pst-common-builder
DEV_TAG		?= 0.10.5
PROJECT 	?=ska-pst-smrb
DEV_USER	?=root
PIV_COMMAND	=/opt/bin/ska_pst_smrb_core -h

# Variables populated for local development and oci build.
# 	Overriden by CI variables. See .gitlab-cy.yml#L7
PST_OCI_COMMON_REGISTRY		?= registry.gitlab.com/ska-telescope/pst/ska-pst-common
PST_OCI_COMMON_BUILDER		?= ska-pst-common-builder
PST_OCI_COMMON_TAG		?=${DEV_TAG}
OCI_IMAGE_BUILD_CONTEXT		?=$(PWD)
OCI_BUILD_ADDITIONAL_ARGS	?=--build-arg BUILD_IMAGE=${PST_OCI_COMMON_REGISTRY}/${PST_OCI_COMMON_BUILDER}:${PST_OCI_COMMON_TAG}

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

ifeq (local-docker-run,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

# local-docker-run:
#	docker run -it --rm -v $(PWD):/mnt/$(PROJECT) -t $(strip $(OCI_IMAGE)):$(VERSION) $(RUN_ARGS)

.PHONY: local-docker-build local-docker-run

BUILD_PATH=$(PWD)/build
SOURCE_PATH=$(PWD)
PROCESSOR_COUNT=`nproc`

.PHONY: gcovr_html gcovr_xml
gcovr_html:
	gcovr  -r ./ -e 'src/apps/.*' -e 'resources/.*' -e '.*/CompilerIdCXX/.*' -e '.*/tests/.*' --exclude-unreachable-branches --exclude-throw-branches --html --html-details -o build/code-coverage.html

gcovr_xml:
	gcovr -r ./ -e 'src/apps/.*' -e 'resources/.*' -e '.*/CompilerIdCXX/.*' -e '.*/tests/.*' --exclude-unreachable-branches --exclude-throw-branches --xml -o build/code-coverage.xml

# Extend pipeline machinery targets
.PHONY: docs-pre-build
docs-pre-build:
	@rm -rf docs/build/*

_VENV=.venv

local-apt-install-dependencies:
	apt-get update -y
	apt-get install -y `cat dependencies/apt.txt`
	pip3 install -r docs/requirements.txt


PROTOBUF_DIR=$(PWD)/protobuf
GENERATED_PATH=$(PWD)/generated

local_generate_code:
	@echo "Generating Python gRPC/Protobuf code."
	@echo "PROTOBUF_DIR=$(PROTOBUF_DIR)"
	@echo "GENERATED_PATH=$(GENERATED_PATH)"
	@echo
	@echo "List of protobuf files: $(shell find "$(PROTOBUF_DIR)" -iname "*.proto")"
	@echo
	@echo "Ensuring generated path $(GENERATED_PATH) exists"
	mkdir -p $(GENERATED_PATH)
	@echo
	protoc --proto_path="$(PROTOBUF_DIR)" \
		--cpp_out="$(GENERATED_PATH)" \
		--grpc_cpp_out="$(GENERATED_PATH)" \
		--plugin=protoc-gen-grpc_cpp=$(shell which grpc_cpp_plugin) \
		$(shell find -L "$(PROTOBUF_DIR)" -iname "*.proto")
	@echo
	@echo "Files generated. $(shell find "$(GENERATED_PATH)/" -iname "*.py")"

.PHONY: local_generate_code

# BDD
.PHONY: ci_simulation_bdd bdd_create_namespace bdd_delete_namespace \
	bdd_apply_manifest bdd_delete_manifest bdd_wait_resources bdd_get_resources
K8S_MANIFEST=manifests.yaml
K8S_WAIT_BDD=--for=condition=ready --timeout=360s pod -l bdd-test=ska-pst-smrb
define bdd_generate_manifest
	@echo "bdd_generate_manifest"
	$(MAKE) K8S_CHART_PARAMS="$(1)" k8s-template-chart
endef
define bdd_delete_manifest:
	@echo "bdd_delete_manifest"
	stat $(K8S_MANIFEST)
	rm $(K8S_MANIFEST)
endef
define bdd_create_namespace
	@echo "bdd_create_namespace"
	kubectl create ns $(KUBE_NAMESPACE)
endef

define bdd_delete_namespace
	@echo "bdd_delete_namespace"
	kubectl delete ns $(KUBE_NAMESPACE) || True
endef
define bdd_apply_manifest
	@echo "bdd_apply_manifest"
	kubectl -n $(KUBE_NAMESPACE) apply -f $(K8S_MANIFEST)
endef
define bdd_delete_manifest
	@echo "bdd_delete_manifest"
	kubectl -n $(KUBE_NAMESPACE) delete -f $(K8S_MANIFEST) --wait=true
endef
define bdd_wait_resources
	@echo "bdd_wait_resources"
	kubectl -n $(KUBE_NAMESPACE) wait $(K8S_WAIT_BDD)
endef
define bdd_get_resources
	@echo "bdd_get_resources"
	kubectl -n $(KUBE_NAMESPACE) get -f $(K8S_MANIFEST)
endef

.PHONY: bdd_pytest bdd_pytest bdd_redeploy
bdd_pytest:
	@apt update -y
	@apt install -y python3 python3-pip
	@pip3 install pytest pytest_bdd
	@cd tests/bdd && pytest -s -rpP -vvv smrb-tests.py

bdd_k8s_pytest:
	@kubectl -n $(KUBE_NAMESPACE) get all
	@kubectl create ns $(KUBE_NAMESPACE)
	@cd tests/bdd && kubectl -n $(KUBE_NAMESPACE) apply -f k8s-tc-1-valid.yaml
	@cd tests/bdd && pytest -s -rpP -k "reserve" -vvv k8s-tests.py
	@cd tests/bdd && pytest -s -rpP -k "delete" -vvv k8s-tests.py
	@kubectl delete ns $(KUBE_NAMESPACE)

bdd_k8s_deploy:
	$(call bdd_generate_manifest,--values=tests/integration/k8s-test/bdd-component.yaml)
	$(call bdd_create_namespace)
	$(call bdd_apply_manifest)
	$(call bdd_wait_resources)
	$(call bdd_get_resources)

bdd_k8s_cleanup:
	$(call bdd_delete_manifest)

bdd_redeploy:
	@echo "Test: Deleting a pod releases resources reserved by ska_pst_smrb_core"
	$(call bdd_generate_manifest,--values=tests/integration/k8s-test/bdd-component.yaml)
	$(call bdd_create_namespace)
	$(call bdd_apply_manifest)
	$(call bdd_wait_resources)
	$(call bdd_get_resources)
	$(call bdd_delete_manifest)
	$(call bdd_create_namespace)
	$(call bdd_apply_manifest)
	$(call bdd_wait_resources)
	$(call bdd_get_resources)
	$(call bdd_delete_manifest) # Manifest contains namespace
	@
	@

ci_simulation_bdd:
	@$(MAKE) bdd_pytest
	@$(MAKE) bdd_redeploy
