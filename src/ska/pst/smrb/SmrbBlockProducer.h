/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/BlockProducer.h"
#include "ska/pst/smrb/DataBlockView.h"

#include <memory>

#ifndef __SKA_PST_SMRB_SmrbBlockProducer_h
#define __SKA_PST_SMRB_SmrbBlockProducer_h

namespace ska::pst::smrb
{

  /**
   * @brief Loads blocks of data from file.
   *
   * Currently maps the entire file into memory as a single block.
   */
  class SmrbBlockProducer : public ska::pst::common::BlockProducer
  {
  public:
    /**
     * @brief Construct a new SmrbBlockProducer object.
     *
     * @param key 4-character hexidecimal string for a PSRDADA shared memory ring buffer
     */
    SmrbBlockProducer(const std::string &key);

    /**
     * @brief Destroy a SmrbBlockProducer object.
     *
     */
    ~SmrbBlockProducer();

    /**
     * @brief Connect to the shared memory ring buffer and read the beam configuration
     *
     * Call this method during configure_scan in an application
     *
     */
    void connect(int timeout);

    /**
     * @brief Open the shared memory ring buffer for viewing and read the scan configuration
     *
     * Call this method at the start of perform_scan in an application
     *
     */
    void open();

    /**
     * @brief Close the shared memory ring buffer
     *
     */
    void close();

    /**
     * @brief Disconnect from the shared memory ring buffer
     *
     */
    void disconnect();

    /**
     * @brief Get the AsciiHeader that is parsed from the header block
     *
     * @return const ska::pst::common::AsciiHeader& header parsed from the header block
     */
    const ska::pst::common::AsciiHeader &get_header() const override;

    /**
     * @brief Get the next block of data
     *
     * @return ska::pst::common::BlockProducer object representing the base address and size, in bytes, of a block of data
     *
     * At the end of the data stream, the base address is set to nullptr and the size is set to zero
     */
    ska::pst::common::BlockProducer::Block next_block() override;

    /**
     * @brief Get the index of the current buffer of the SMRB data block
     *
     */
    size_t get_current_buffer_index() const;

    /**
     * @brief Go to the specified buffer of the SMRB data block
     *
     * @param buffer_index ring buffer index in the data block
     */
    void seek_to_buffer_index(size_t buffer_index);

    /**
     * @brief Get the obs_offset of the current_block, from the start of the scan, in bytes.
     *
     * @return size_t offset from the start of the scan in bytes.
     */
    size_t get_obs_offset() const { return current_block.obs_offset; }

  protected:
    //! the shared memory ring buffer viewer
    std::unique_ptr<ska::pst::smrb::DataBlockView> viewer;

    //! the header parsed from the header block of the shared memory ring buffer
    ska::pst::common::AsciiHeader header;

    //! the current block
    ska::pst::common::BlockProducer::Block current_block;

    //! when true, seek to the end of the ring buffer at start of next_block
    bool seek_to_end{true};
  };

} // namespace ska::pst::smrb

#endif // __SKA_PST_SMRB_SmrbBlockProducer_h
