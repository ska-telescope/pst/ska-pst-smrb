/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <queue>

#include "ska/pst/smrb/DataBlockAccessor.h"

#ifndef __SKA_PST_SMRB_DataBlockRead_h
#define __SKA_PST_SMRB_DataBlockRead_h

namespace ska {
namespace pst {
namespace smrb {

    /**
   * @brief Object Oriented interface to read to a PSRDADA Shared Memory Ring Buffer (SMRB).
   * 
   */
  class DataBlockRead : public DataBlockAccessor
  {
    public:
     /**
       * @brief Construct a new Data Block Read object for the SMRB identified by the key.
       * 
       * @param key 4 character hexidecimal key that identifies the SMRB in shared memory.
       */
      explicit DataBlockRead(const std::string &key);

       /**
       * @brief Destroy the Data Block Read object.
       * Closes the access to the SMRB if required
       */
      ~DataBlockRead();

      /**
       * @brief Open the SMRB for reading
       * 
       */
      void open() override;

      /**
       * @brief Lock read access to the SMRB
       * 
       */
      void lock() override;

      /**
       * @brief Close the connection to the SMRB
       * 
       */
      void close() override;

      /**
       * @brief Unlock read access to the SMRB
       * 
       */
      void unlock() override;

      /**
       * @brief Read the scan configuration from the header_block
       * 
       */
      void read_config();

      /**
       * @brief Read the complete header from the header_block
       * 
       */
      void read_header();

      /**
       * @brief Read requested nbytes from the datablock into the provided buffer
       * 
       * @param buffer 
       * @param nbytes 
       * @return ssize_t 
       */
      ssize_t read_data(char * buffer, size_t nbytes);
      
      /**
       * @brief Open a data_block buffer for DMA
       * 
       * @return void* pointer to the opened data_block buffer
       */
      char * open_block() override;

      /**
       * @brief Close an opened data_block buffer specifying the number of bytes read
       * @param bytes number of bytes read to the opened data_block buffer
       * @return ssize_t number of bytes read
       */
      ssize_t close_block(uint64_t bytes) override;

      /**
       * @brief Update the number of bytes read to the opened data_block buffer
       * 
       * @param bytes number of bytes read
       * @return ssize_t number of bytes read
       */
      ssize_t update_block(uint64_t bytes);

      /**
       * @brief zero the buffer in the data_block after the currently opened buffer 
       * 
       */
      void zero_next_block();

      /**
       * @brief Check if the End-Of-Data flag on the data block
       * 
       * @return true if the End-Of-Data flag has been raised
       */
      bool check_eod();

    protected:

    private:

  };
  
}
}
}

#endif // __SKA_PST_SMRB_DataBlockRead_h