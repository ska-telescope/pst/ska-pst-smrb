/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/tests/DataBlockReadTest.h"
#include "ska/pst/smrb/DataBlockRead.h"
#include "ska/pst/smrb/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::smrb::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

DataBlockReadTest::DataBlockReadTest()
  : ::testing::Test()
{
}

void DataBlockReadTest::SetUp()
{
  _db = std::make_shared<DataBlockCreate>("dada"); // NOLINT
  _db->create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT

  scan_config.load_from_file(test_data_file("scan_config.txt")); // NOLINT
  full_header.load_from_file(test_data_file("full_header.txt")); // NOLINT

  _writer = std::make_shared<DataBlockWrite>("dada"); // NOLINT
  _writer->connect(1); // NOLINT
  _writer->lock(); // NOLINT
  _writer->write_config(scan_config.raw()); // NOLINT
  _writer->write_header(full_header.raw()); // NOLINT
}

void DataBlockReadTest::write_data_to_block(size_t nbytes)
{
  _writer->open(); // NOLINT
  std::vector<char> data(nbytes); // NOLINT
  _writer->write_data(&data[0], nbytes); // NOLINT
  _writer->close(); // NOLINT
}

void DataBlockReadTest::TearDown()
{
  if (_writer)
  {
    if (_writer->get_opened())
    {
      _writer->close(); // NOLINT
    }
    if (_writer->get_locked())
    {
      _writer->unlock(); // NOLINT
    }
    _writer->disconnect(); // NOLINT
  }
  _writer = nullptr; // NOLINT

  if (_db)
  {
    _db->destroy(); // NOLINT
  }
  _db = nullptr; // NOLINT
}

TEST_F(DataBlockReadTest, test_construct_delete) // NOLINT
{
  DataBlockRead db("eeee"); // NOLINT
}

TEST_F(DataBlockReadTest, test_common_operations) // NOLINT
{
  DataBlockRead db("dada"); // NOLINT

  EXPECT_THROW(db.close(), std::runtime_error); // NOLINT
  EXPECT_THROW(db.unlock(), std::runtime_error); // NOLINT
  EXPECT_THROW(db.disconnect(), std::runtime_error); // NOLINT

  // transition through the connect -> lock -> read_config -> read_header -> open sequence
  db.connect(0); // NOLINT
  EXPECT_THROW(db.connect(0), std::runtime_error); // NOLINT
  db.lock(); // NOLINT
  EXPECT_THROW(db.lock(), std::runtime_error); // NOLINT
  db.read_config(); // NOLINT
  EXPECT_THROW(db.read_config(), std::runtime_error); // NOLINT
  db.read_header(); // NOLINT
  EXPECT_THROW(db.read_header(), std::runtime_error); // NOLINT
  db.open(); // NOLINT
  EXPECT_THROW(db.open(), std::runtime_error); // NOLINT

  // transition back to disconnected
  EXPECT_THROW(db.unlock(), std::runtime_error); // NOLINT
  db.close(); // NOLINT
  EXPECT_THROW(db.close(), std::runtime_error); // NOLINT
  db.unlock(); // NOLINT
  EXPECT_THROW(db.unlock(), std::runtime_error); // NOLINT
  db.disconnect(); // NOLINT
  EXPECT_THROW(db.disconnect(), std::runtime_error); // NOLINT
}

TEST_F(DataBlockReadTest, test_read_data) // NOLINT
{
  static constexpr size_t nbytes = 1024; // NOLINT
  write_data_to_block(nbytes); // NOLINT

  DataBlockRead db("dada"); // NOLINT
  db.connect(0); // NOLINT
  db.lock(); // NOLINT
  db.read_config(); // NOLINT
  db.read_header(); // NOLINT
  db.open(); // NOLINT
  std::vector<char> out_data(nbytes); // NOLINT
  EXPECT_EQ(db.read_data(&out_data[0], nbytes), nbytes); // NOLINT
  db.close(); // NOLINT
  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockReadTest, test_read_config_and_header) // NOLINT
{
  DataBlockRead db("dada"); // NOLINT

  db.connect(0); // NOLINT
  EXPECT_THROW(db.read_config(), std::runtime_error); // NOLINT
  EXPECT_THROW(db.read_header(), std::runtime_error); // NOLINT
  db.lock(); // NOLINT

  db.read_config(); // NOLINT
  EXPECT_EQ(db.get_config(), scan_config.raw()); // NOLINT
  EXPECT_THROW(db.read_config(), std::runtime_error); // NOLINT

  db.read_header(); // NOLINT
  EXPECT_EQ(db.get_header(), full_header.raw()); // NOLINT
  EXPECT_THROW(db.read_header(), std::runtime_error); // NOLINT

  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockReadTest, test_open_block) // NOLINT
{
  static constexpr size_t nbytes = 1024; // NOLINT
  write_data_to_block(nbytes); // NOLINT

  DataBlockRead db("dada"); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  db.connect(0); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  db.lock(); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  db.read_config(); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT
  db.read_header(); // NOLINT
  db.open(); // NOLINT

  EXPECT_NO_THROW(db.open_block()); // NOLINT
  EXPECT_THROW(db.open_block(), std::runtime_error); // NOLINT

  db.close(); // NOLINT
  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockReadTest, test_close_block) // NOLINT
{
  static constexpr size_t nbytes = 1024; // NOLINT
  write_data_to_block(nbytes); // NOLINT

  DataBlockRead db("dada"); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.connect(0); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.lock(); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.read_config(); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.read_header(); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT
  db.open(); // NOLINT
  EXPECT_THROW(db.close_block(0), std::runtime_error); // NOLINT

  db.open_block(); // NOLINT
  uint64_t bytes_read = db.get_buf_bytes(); // NOLINT
  db.close_block(bytes_read); // NOLINT

  db.close(); // NOLINT

  // close should clear only the header
  db.check_have_header(false);
  db.check_have_config(true);

  db.clear_config();
  db.check_have_config(false);

  db.unlock(); // NOLINT
  db.disconnect(); // NOLINT
}

TEST_F(DataBlockReadTest, test_eod_on_read) // NOLINT
{
  static constexpr size_t nbytes = 1024; // NOLINT
  write_data_to_block(nbytes); // NOLINT

  DataBlockRead db("dada"); // NOLINT
  db.connect(0); // NOLINT
  db.lock(); // NOLINT
  db.read_config(); // NOLINT
  db.read_header(); // NOLINT
  db.open(); // NOLINT

  std::vector<char> out_data(nbytes); // NOLINT
  SPDLOG_TRACE("ska::pst::smrb::test::DataBlockReadTest::test_eod_on_read reading written data"); // NOLINT
  EXPECT_EQ(db.read_data(&out_data[0], nbytes), nbytes); // NOLINT

  SPDLOG_TRACE("ska::pst::smrb::test::DataBlockReadTest::test_eod_on_read trying to read when no data should exist"); // NOLINT
  EXPECT_EQ(db.read_data(&out_data[0], nbytes), 0); // NOLINT
}

TEST_F(DataBlockReadTest, test_eod_on_read_block) // NOLINT
{
  auto nbytes = size_t(data_bufsz); // NOLINT
  write_data_to_block(nbytes); // NOLINT

  DataBlockRead db("dada"); // NOLINT
  db.connect(0); // NOLINT
  db.lock(); // NOLINT
  db.read_config(); // NOLINT
  db.read_header(); // NOLINT
  db.open(); // NOLINT

  SPDLOG_TRACE("ska::pst::smrb::test::DataBlockReadTest::test_eod_on_read reading written data"); // NOLINT
  auto block = db.open_block(); // NOLINT
  EXPECT_NE(block, nullptr); // NOLINT
  EXPECT_EQ(db.get_buf_bytes(), nbytes); // NOLINT
  db.close_block(nbytes); // NOLINT

  block = db.open_block(); // NOLINT
  EXPECT_EQ(block, nullptr); // NOLINT
  EXPECT_EQ(db.get_buf_bytes(), 0); // NOLINT
}

} // namespace ska::pst::smrb::test
