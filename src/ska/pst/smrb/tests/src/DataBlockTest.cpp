/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/tests/DataBlockTest.h"
#include "ska/pst/smrb/DataBlock.h"
#include "ska/pst/smrb/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::smrb::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

DataBlockTest::DataBlockTest()
    : ::testing::Test()
{
}

void DataBlockTest::SetUp()
{
    SPDLOG_TRACE("DataBlockTest::SetUp std::make_shared<DataBlockCreate>('dada')"); // NOLINT
    _db = std::make_shared<DataBlockCreate>("dada"); // NOLINT
    _db->create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT
}

void DataBlockTest::TearDown()
{
    if (_db) {
        SPDLOG_TRACE("DataBlockTest::TearDown _db->destroy"); // NOLINT
        _db->destroy(); // NOLINT
    }
    SPDLOG_TRACE("DataBlockTest::TearDown dereference _db"); // NOLINT
    _db = nullptr; // NOLINT
    SPDLOG_TRACE("DataBlockTest::TearDown done"); // NOLINT
}

TEST_F(DataBlockTest, construct_delete) // NOLINT
{
    DataBlock db ("eeee"); // NOLINT
}

TEST_F(DataBlockTest, connect_disconnect) // NOLINT
{
    // test connection
    DataBlock db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.disconnect(); // NOLINT

    // test with timeout
    db.connect(1); // NOLINT
    db.disconnect(); // NOLINT

    // test that connecting a second time throws an error
    db.connect(0); // NOLINT
    EXPECT_THROW(db.connect(0), std::runtime_error); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockTest, connect_timeout) // NOLINT
{
    DataBlock db("eeee"); // NOLINT
    EXPECT_THROW(db.connect(1), std::runtime_error); // NOLINT
}

TEST_F(DataBlockTest, test_db_topology) // NOLINT
{
    DataBlock db("dada"); // NOLINT
    db.connect(0); // NOLINT
    EXPECT_EQ(db.get_header_nbufs(), hdr_nbufs); // NOLINT
    EXPECT_EQ(db.get_header_bufsz(), hdr_bufsz); // NOLINT
    EXPECT_EQ(db.get_data_nbufs(), data_nbufs); // NOLINT
    EXPECT_EQ(db.get_data_bufsz(), data_bufsz); // NOLINT
    EXPECT_EQ(db.get_device(), device_id); // NOLINT
}

TEST_F(DataBlockTest, test_psrdada_key_parsing) // NOLINT
{
    // check that these invalid PSRDADA keys throw an exception
    EXPECT_THROW(DataBlock::parse_psrdada_key("abc"), std::runtime_error); // NOLINT
    EXPECT_THROW(DataBlock::parse_psrdada_key("abcde"), std::runtime_error); // NOLINT
    EXPECT_THROW(DataBlock::parse_psrdada_key("gggg"), std::runtime_error); // NOLINT
    EXPECT_EQ(DataBlock::parse_psrdada_key("0000"), key_t(0)); // NOLINT
}

TEST_F(DataBlockTest, test_page) // NOLINT
{
    DataBlock db("dada"); // NOLINT
    db.connect(0); // NOLINT
    db.page(); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockTest, test_nreaders) // NOLINT
{
    DataBlock db("dada"); // NOLINT
    db.connect(0); // NOLINT
    EXPECT_EQ(db.get_nreaders(), num_readers); // NOLINT
    db.disconnect(); // NOLINT
}

TEST_F(DataBlockTest, test_shm_lock) // NOLINT
{
    DataBlockCreate small_db("eada"); // NOLINT
    static constexpr uint64_t small_bufsz = 16384; // NOLINT
    static constexpr uint64_t small_nbufs = 2; // NOLINT
    small_db.create(small_nbufs, small_bufsz, small_nbufs, small_bufsz, num_readers, device_id); // NOLINT

    DataBlock db("eada"); // NOLINT
    db.connect(0); // NOLINT
    db.lock_memory(); // NOLINT
    db.unlock_memory(); // NOLINT
    db.disconnect(); // NOLINT
}

} // namespace ska::pst::smrb::test