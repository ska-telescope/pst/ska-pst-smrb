/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/tests/DataBlockCreateTest.h"
#include "ska/pst/smrb/DataBlockCreate.h"
#include "ska/pst/smrb/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::smrb::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

DataBlockCreateTest::DataBlockCreateTest()
    : ::testing::Test()
{
}

TEST_F(DataBlockCreateTest, test_construct_delete) // NOLINT
{
    DataBlockCreate db("eeee"); // NOLINT
}

TEST_F(DataBlockCreateTest, test_create_destroy) // NOLINT
{
    DataBlockCreate db("dada"); // NOLINT
    EXPECT_FALSE(db.is_connected()); // NOLINT

    db.create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT
    EXPECT_TRUE(db.is_connected()); // NOLINT

    EXPECT_EQ(db.get_header_nbufs(), hdr_nbufs); // NOLINT
    EXPECT_EQ(db.get_header_bufsz(), hdr_bufsz); // NOLINT
    EXPECT_EQ(db.get_data_nbufs(), data_nbufs); // NOLINT
    EXPECT_EQ(db.get_data_bufsz(), data_bufsz); // NOLINT
    EXPECT_EQ(db.get_device(), device_id); // NOLINT

    db.destroy(); // NOLINT
    EXPECT_FALSE(db.is_connected()); // NOLINT

    EXPECT_EQ(db.get_header_nbufs(), 0); // NOLINT
    EXPECT_EQ(db.get_header_bufsz(), 0); // NOLINT
    EXPECT_EQ(db.get_data_nbufs(), 0); // NOLINT
    EXPECT_EQ(db.get_data_bufsz(), 0); // NOLINT
}

TEST_F(DataBlockCreateTest, test_create_when_already_created) // NOLINT
{
    DataBlockCreate db("eeee"); // NOLINT
    db.create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT

    // check we can't create it again until it has been destroyed
    EXPECT_THROW(db.create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id), std::runtime_error); // NOLINT

    db.destroy(); // NOLINT
    EXPECT_FALSE(db.is_connected()); // NOLINT

    db.create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT
    EXPECT_TRUE(db.is_connected()); // NOLINT

    db.destroy(); // NOLINT
    EXPECT_FALSE(db.is_connected()); // NOLINT
}

TEST_F(DataBlockCreateTest, test_create_destroy_released) // NOLINT
{
    DataBlockCreate db("dada"); // NOLINT
    db.create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT

    uint32_t shm_bytes_used = get_shared_memory_bytes_used();
    SPDLOG_TRACE("DataBlockCreateTest::test_create_destroy_released shm_bytes_used={}", shm_bytes_used);

    uint32_t constexpr buffer_sync_size_bytes = 5;
    uint32_t constexpr ipcbuf_sync_size_bytes = 528;
    uint32_t hdr_sync_size = ipcbuf_sync_size_bytes + (hdr_nbufs * buffer_sync_size_bytes);
    uint32_t dat_sync_size = ipcbuf_sync_size_bytes + (data_nbufs * buffer_sync_size_bytes);
    uint32_t expected_size = hdr_sync_size + (hdr_nbufs * hdr_bufsz) + dat_sync_size + (data_nbufs * data_bufsz);

    EXPECT_EQ(shm_bytes_used, expected_size); // NOLINT

    db.destroy(); // NOLINT

    // confirm that shared memory segments now no longer exist
    shm_bytes_used = get_shared_memory_bytes_used();
    SPDLOG_TRACE("DataBlockCreateTest::test_create_destroy_released shm_bytes_used={}", shm_bytes_used);

    // now the expected size of all shared memory allocated to this process should be zero bytes
    EXPECT_EQ(shm_bytes_used, 0); // NOLINT
}

} // namespace ska::pst::smrb::test