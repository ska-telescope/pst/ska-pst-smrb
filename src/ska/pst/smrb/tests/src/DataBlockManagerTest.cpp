/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <thread>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/Timer.h"
#include "ska/pst/common/statemodel/StateModel.h"

#include "ska/pst/smrb/testutils/GtestMain.h"

#include "ska/pst/smrb/DataBlockWrite.h"
#include "ska/pst/smrb/DataBlockRead.h"
#include "ska/pst/smrb/DataBlockManager.h"

#include "ska/pst/smrb/tests/DataBlockManagerTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::smrb::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

DataBlockManagerTest::DataBlockManagerTest() // NOLINT
    : ::testing::Test()
{
}

void DataBlockManagerTest::modify_after_sleep(int delay_ms, volatile bool * signal) // NOLINT
{
    static constexpr int microseconds_per_millisecond = 1000; // NOLINT
    int delay_us = delay_ms * microseconds_per_millisecond; // NOLINT
    usleep(delay_us); // NOLINT
    *signal = true; // NOLINT
}

void DataBlockManagerTest::SetUp()
{
    SPDLOG_TRACE("ska::pst::smrb::test::DataBlockManagerTest::SetUp"); // NOLINT
    dbm = std::make_shared<DataBlockManager>(); // NOLINT
}

void DataBlockManagerTest::TearDown() // NOLINT
{
    SPDLOG_TRACE("ska::pst::smrb::test::DataBlockManagerTest::TearDown"); // NOLINT
    dbm->quit(); // NOLINT
    dbm = nullptr; // NOLINT
}

TEST_F(DataBlockManagerTest, test_happy_path) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_happy_path"); // NOLINT

    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
    // config initialisation
    ska::pst::common::AsciiHeader config; // NOLINT
    config.set_val("DATA_KEY","a000"); // NOLINT
    config.set_val("WEIGHTS_KEY","a010"); // NOLINT
    config.set_val("HB_NBUFS","8"); // NOLINT
    config.set_val("HB_BUFSZ","4096"); // NOLINT
    config.set_val("DB_NBUFS","8"); // NOLINT
    config.set_val("DB_BUFSZ","1048576"); // NOLINT
    config.set_val("WB_NBUFS","8"); // NOLINT
    config.set_val("WB_BUFSZ","8192"); // NOLINT

    EXPECT_TRUE(dbm->is_idle());
    EXPECT_FALSE(dbm->is_beam_configured());
    EXPECT_FALSE(dbm->is_scan_configured());
    EXPECT_FALSE(dbm->is_scanning());

    // validate ConfigureBeam configuration
    dbm->configure_beam(config); // NOLINT
    dbm->print_statistics(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
    ASSERT_TRUE(dbm->is_beam_configured());
    EXPECT_FALSE(dbm->is_scan_configured());
    EXPECT_FALSE(dbm->is_scanning());

    dbm->configure_scan(config); // NOLINT
    dbm->print_statistics(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::ScanConfigured, dbm->get_state()); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured());
    ASSERT_TRUE(dbm->is_scan_configured());
    EXPECT_FALSE(dbm->is_scanning());

    dbm->start_scan(config); // NOLINT
    dbm->print_statistics(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Scanning, dbm->get_state()); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured());
    EXPECT_TRUE(dbm->is_scan_configured());
    ASSERT_TRUE(dbm->is_scanning());

    dbm->stop_scan(); // NOLINT
    dbm->print_statistics(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::ScanConfigured, dbm->get_state()); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured());
    EXPECT_TRUE(dbm->is_scan_configured());
    ASSERT_FALSE(dbm->is_scanning());

    dbm->deconfigure_scan(); // NOLINT
    dbm->print_statistics(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured());
    ASSERT_FALSE(dbm->is_scan_configured());
    EXPECT_FALSE(dbm->is_scanning());

    dbm->deconfigure_beam(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
    ASSERT_FALSE(dbm->is_beam_configured());
    EXPECT_FALSE(dbm->is_scan_configured());
    EXPECT_FALSE(dbm->is_scanning());
}


TEST_F(DataBlockManagerTest, test_configure_beam_from_file) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_configure_beam_from_file"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    EXPECT_FALSE(dbm->is_beam_configured()); // NOLINT

    dbm->configure_beam_from_file(test_data_file("beam_config.txt")); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured()); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT

    DataBlockStats::stats_t dhs = dbm->get_data_header_stats(); // NOLINT
    DataBlockStats::stats_t dds = dbm->get_data_data_stats(); // NOLINT
    DataBlockStats::stats_t whs = dbm->get_weights_header_stats(); // NOLINT
    DataBlockStats::stats_t wds = dbm->get_weights_data_stats(); // NOLINT

    EXPECT_EQ(dhs.nbufs, 8); // NOLINT
    EXPECT_EQ(dhs.bufsz, 4096); // NOLINT
    EXPECT_EQ(dhs.written, 0); // NOLINT
    EXPECT_EQ(dhs.read, 0); // NOLINT
    EXPECT_EQ(dhs.full, 0); // NOLINT
    EXPECT_EQ(dhs.clear, 0); // NOLINT
    EXPECT_EQ(dhs.available, 8); // NOLINT

    EXPECT_EQ(whs.nbufs, 8); // NOLINT
    EXPECT_EQ(whs.bufsz, 4096); // NOLINT
    EXPECT_EQ(whs.written, 0); // NOLINT
    EXPECT_EQ(whs.read, 0); // NOLINT
    EXPECT_EQ(whs.full, 0); // NOLINT
    EXPECT_EQ(whs.clear, 0); // NOLINT
    EXPECT_EQ(whs.available, 8); // NOLINT

    EXPECT_EQ(dds.nbufs, 8); // NOLINT
    EXPECT_EQ(dds.bufsz, 1048576); // NOLINT
    EXPECT_EQ(dds.written, 0); // NOLINT
    EXPECT_EQ(dds.read, 0); // NOLINT
    EXPECT_EQ(dds.full, 0); // NOLINT
    EXPECT_EQ(dds.clear, 0); // NOLINT
    EXPECT_EQ(dds.available, 8); // NOLINT

    EXPECT_EQ(wds.nbufs, 8); // NOLINT
    EXPECT_EQ(wds.bufsz, 8192); // NOLINT
    EXPECT_EQ(wds.written, 0); // NOLINT
    EXPECT_EQ(wds.read, 0); // NOLINT
    EXPECT_EQ(wds.full, 0); // NOLINT
    EXPECT_EQ(wds.clear, 0); // NOLINT
    EXPECT_EQ(wds.available, 8); // NOLINT
}


TEST_F(DataBlockManagerTest, test_validate_configure_beam_valid_configuration) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_validate_configure_beam_valid_configuration"); // NOLINT

    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    // config initialisation
    ska::pst::common::AsciiHeader config; // NOLINT
    config.set_val("DATA_KEY","a000"); // NOLINT
    config.set_val("WEIGHTS_KEY","a010"); // NOLINT
    config.set_val("HB_NBUFS","8"); // NOLINT
    config.set_val("HB_BUFSZ","4096"); // NOLINT
    config.set_val("DB_NBUFS","8"); // NOLINT
    config.set_val("DB_BUFSZ","1048576"); // NOLINT
    config.set_val("WB_NBUFS","8"); // NOLINT
    config.set_val("WB_BUFSZ","8192"); // NOLINT

    // validate ConfigureBeam configuration
    dbm->configure_beam(config); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
}


TEST_F(DataBlockManagerTest, test_validate_configure_beam_shared_memory_allocation) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_validate_configure_beam_shared_memory_allocation");

    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    static constexpr uint32_t hdr_nbufs = 8;
    static constexpr uint32_t hdr_bufsz = 4096;
    static constexpr uint32_t dat_nbufs = 5;
    static constexpr uint32_t dat_bufsz = 1048576;
    static constexpr uint32_t wts_nbufs = 6;
    static constexpr uint32_t wts_bufsz = 8192;

    // config initialisation
    ska::pst::common::AsciiHeader config;
    config.set_val("DATA_KEY", "a000");
    config.set_val("WEIGHTS_KEY", "a010");
    config.set("HB_NBUFS", hdr_nbufs);
    config.set("HB_BUFSZ", hdr_bufsz);
    config.set("DB_NBUFS", dat_nbufs);
    config.set("DB_BUFSZ", dat_bufsz);
    config.set("WB_NBUFS", wts_nbufs);
    config.set("WB_BUFSZ", wts_bufsz);

    uint32_t shm_bytes_used = get_shared_memory_bytes_used();
    SPDLOG_TRACE("DataBlockManagerTest::test_validate_configure_beam_shared_memory_allocation shm_bytes_used={}", shm_bytes_used);
    ASSERT_EQ(shm_bytes_used, 0);

    // configure the beam which results in shared memory allocation
    dbm->configure_beam(config); // NOLINT

    shm_bytes_used = get_shared_memory_bytes_used();
    SPDLOG_TRACE("DataBlockManagerTest::test_validate_configure_beam_shared_memory_allocation shm_bytes_used={}", shm_bytes_used);

    uint32_t constexpr buffer_sync_size_bytes = 5;
    uint32_t constexpr ipcbuf_sync_size_bytes = 528;
    uint32_t hdr_sync_size = ipcbuf_sync_size_bytes + (hdr_nbufs * buffer_sync_size_bytes);
    uint32_t dat_sync_size = ipcbuf_sync_size_bytes + (dat_nbufs * buffer_sync_size_bytes);
    uint32_t wts_sync_size = ipcbuf_sync_size_bytes + (wts_nbufs * buffer_sync_size_bytes);
    uint32_t expected_size =  \
        hdr_sync_size + (hdr_nbufs * hdr_bufsz) + \
        dat_sync_size + (dat_nbufs * dat_bufsz) + \
        hdr_sync_size + (hdr_nbufs * hdr_bufsz) + \
        wts_sync_size + (wts_nbufs * wts_bufsz);

    ASSERT_EQ(shm_bytes_used, expected_size);

    dbm->deconfigure_beam(); // NOLINT

    shm_bytes_used = get_shared_memory_bytes_used();
    SPDLOG_TRACE("DataBlockManagerTest::test_validate_configure_beam_shared_memory_allocation shm_bytes_used={}", shm_bytes_used);
    ASSERT_EQ(shm_bytes_used, 0);
}


TEST_F(DataBlockManagerTest, test_validate_configure_beam_missing_keys) // NOLINT
{
    // config initialisation
    ska::pst::common::AsciiHeader config; // NOLINT
    config.set_val("DATA_KEY","a000"); // NOLINT
    config.set_val("HB_NBUFS","8"); // NOLINT
    config.set_val("DB_NBUFS","8"); // NOLINT
    config.set_val("WB_NBUFS","8"); // NOLINT

    // validate ConfigureBeam configuration
    EXPECT_THROW(dbm->configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

}

TEST_F(DataBlockManagerTest, test_validate_configure_beam_invalid_key_pattern) // NOLINT
{
    // config initialisation
    ska::pst::common::AsciiHeader config; // NOLINT
    config.set_val("DATA_KEY","@!00"); // NOLINT
    config.set_val("WEIGHTS_KEY","a0@!"); // NOLINT
    config.set_val("HB_NBUFS","8"); // NOLINT
    config.set_val("HB_BUFSZ","4096"); // NOLINT
    config.set_val("DB_NBUFS","8"); // NOLINT
    config.set_val("DB_BUFSZ","1048576"); // NOLINT
    config.set_val("WB_NBUFS","8"); // NOLINT
    config.set_val("WB_BUFSZ","8192"); // NOLINT

    // validate ConfigureBeam configuration
    EXPECT_THROW(dbm->configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
    config.set_val("DATA_KEY","a@!00"); // NOLINT
    EXPECT_THROW(config.set_val("WEIGHTS_KEY",""), std::runtime_error); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

}

TEST_F(DataBlockManagerTest, test_validate_configure_beam_invalid_buffer_pattern) // NOLINT
{
    // config initialisation
    ska::pst::common::AsciiHeader config; // NOLINT
    config.set_val("DATA_KEY","a000"); // NOLINT
    config.set_val("WEIGHTS_KEY","a010"); // NOLINT
    config.set_val("HB_NBUFS","a8"); // NOLINT
    config.set_val("HB_BUFSZ","4096a"); // NOLINT
    EXPECT_THROW(config.set_val("DB_NBUFS",""), std::runtime_error); // NOLINT
    config.set_val("DB_BUFSZ","1048576!"); // NOLINT
    config.set_val("WB_NBUFS","!8"); // NOLINT
    config.set_val("WB_BUFSZ","8192"); // NOLINT

    // validate ConfigureBeam configuration
    EXPECT_THROW(dbm->configure_beam(config), ska::pst::common::pst_validation_error); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

}

TEST_F(DataBlockManagerTest, test_print_statistics_error_on_idle) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_print_statistics_error_on_idle"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    EXPECT_THROW(dbm->print_statistics(), std::runtime_error); // NOLINT
    dbm->configure_beam_from_file(test_data_file("beam_config.txt")); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
    dbm->print_statistics(); // NOLINT
}


TEST_F(DataBlockManagerTest, test_wait_for_duration) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_wait_for_duration"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    dbm->configure_beam_from_file(test_data_file("beam_config.txt")); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
    ska::pst::common::Timer timer; // NOLINT
    bool signal = false; // NOLINT

    static constexpr int duration_ms = 200; // NOLINT
    dbm->wait_for_duration(duration_ms, &signal); // NOLINT
    int elapsed_ms = timer.get_elapsed_milliseconds(); // NOLINT
    ASSERT_GE(elapsed_ms, duration_ms); // NOLINT
    ASSERT_LE(elapsed_ms, duration_ms * 2.0); // NOLINT

    timer.reset(); // NOLINT
    static constexpr int delay_ms = 100; // NOLINT
    std::thread delay_thread = std::thread(&DataBlockManagerTest::modify_after_sleep, this, delay_ms, &signal); // NOLINT
    dbm->wait_for_duration(duration_ms, &signal); // NOLINT
    elapsed_ms = timer.get_elapsed_milliseconds(); // NOLINT
    delay_thread.join(); // NOLINT
    ASSERT_GE(elapsed_ms, delay_ms); // NOLINT
    // gitlab runners often see very large delays, hence 10.* multiplier
    ASSERT_LE(elapsed_ms, delay_ms * 10.0); // NOLINT
}


TEST_F(DataBlockManagerTest, test_wait_for_observation) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_wait_for_observation"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    dbm->configure_beam_from_file(test_data_file("beam_config.txt")); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT

    bool signal = true; // NOLINT
    dbm->wait_for_observation(&signal); // NOLINT

    ska::pst::common::AsciiHeader config; // NOLINT
    config.load_from_file(test_data_file("beam_config.txt")); // NOLINT
    std::string data_key = config.get_val("DATA_KEY"); // NOLINT
    std::string weights_key = config.get_val("WEIGHTS_KEY"); // NOLINT

    // declare some input buffers to write/read
    size_t data_bufsz{}, weights_bufsz{}; // NOLINT
    config.get("DB_BUFSZ", &data_bufsz); // NOLINT
    config.get("WB_BUFSZ", &weights_bufsz); // NOLINT
    std::vector<char> data(data_bufsz, 0); // NOLINT
    std::vector<char> weights(weights_bufsz, 0); // NOLINT

    DataBlockWrite data_writer(data_key); // NOLINT
    DataBlockWrite weights_writer(weights_key); // NOLINT

    ska::pst::common::AsciiHeader scan_config; // NOLINT
    ska::pst::common::AsciiHeader full_header; // NOLINT
    scan_config.load_from_file(test_data_file("scan_config.txt")); // NOLINT
    full_header.load_from_file(test_data_file("full_header.txt")); // NOLINT

    data_writer.connect(0); // NOLINT
    data_writer.lock(); // NOLINT
    data_writer.write_config(scan_config.raw()); // NOLINT
    data_writer.write_header(full_header.raw()); // NOLINT
    data_writer.open(); // NOLINT
    weights_writer.connect(0); // NOLINT
    weights_writer.lock(); // NOLINT
    weights_writer.write_config(scan_config.raw()); // NOLINT
    weights_writer.write_header(full_header.raw()); // NOLINT
    weights_writer.open(); // NOLINT

    data_writer.write_data(&data[0], data_bufsz); // NOLINT
    weights_writer.write_data(&weights[0], weights_bufsz); // NOLINT

    data_writer.close(); // NOLINT
    data_writer.unlock(); // NOLINT
    data_writer.disconnect(); // NOLINT
    weights_writer.close(); // NOLINT
    weights_writer.unlock(); // NOLINT
    weights_writer.disconnect(); // NOLINT

    DataBlockRead data_reader(data_key); // NOLINT
    DataBlockRead weights_reader(weights_key); // NOLINT

    data_reader.connect(0); // NOLINT
    data_reader.lock(); // NOLINT
    data_reader.read_config(); // NOLINT
    data_reader.read_header(); // NOLINT
    data_reader.open(); // NOLINT
    weights_reader.connect(0); // NOLINT
    weights_reader.lock(); // NOLINT
    weights_reader.read_config(); // NOLINT
    weights_reader.read_header(); // NOLINT
    weights_reader.open(); // NOLINT

    data_reader.read_data(&data[0], data_bufsz); // NOLINT
    weights_reader.read_data(&weights[0], weights_bufsz); // NOLINT

    data_reader.close(); // NOLINT
    data_reader.unlock(); // NOLINT
    data_reader.disconnect(); // NOLINT
    weights_reader.close(); // NOLINT
    weights_reader.unlock(); // NOLINT
    weights_reader.disconnect(); // NOLINT

    signal = false; // NOLINT
    dbm->wait_for_observation(&signal); // NOLINT
}


TEST_F(DataBlockManagerTest, test_wait_for_signal) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_wait_for_signal"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    dbm->configure_beam_from_file(test_data_file("beam_config.txt")); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT

    ska::pst::common::Timer timer; // NOLINT
    bool signal = true; // NOLINT
    dbm->wait_for_signal(&signal); // NOLINT
    double elapsed_ms = timer.get_elapsed_milliseconds(); // NOLINT
    static constexpr int minimum_sleep_ms = 100; // NOLINT
    ASSERT_GE(elapsed_ms, minimum_sleep_ms); // NOLINT
    ASSERT_LE(elapsed_ms, minimum_sleep_ms* 2.0); // NOLINT

    signal = false; // NOLINT
    static constexpr int delay_ms = 200; // NOLINT
    timer.reset(); // NOLINT
    std::thread delay_thread = std::thread(&DataBlockManagerTest::modify_after_sleep, this, delay_ms, &signal); // NOLINT
    dbm->wait_for_signal(&signal); // NOLINT
    elapsed_ms = timer.get_elapsed_milliseconds(); // NOLINT
    delay_thread.join(); // NOLINT
    ASSERT_GE(elapsed_ms, delay_ms); // NOLINT
    ASSERT_LE(elapsed_ms, delay_ms * 2.0); // NOLINT
}


TEST_F(DataBlockManagerTest, test_get_beam_configuration) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_get_beam_configuration"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    EXPECT_FALSE(dbm->is_beam_configured()); // NOLINT

    ska::pst::common::AsciiHeader config; // NOLINT
    config.set_val("DATA_KEY","a000"); // NOLINT
    config.set_val("WEIGHTS_KEY","a010"); // NOLINT
    // config.set_val("NUMA_NODE","0"); // NOLINT

    // common header ring buffer defintion
    config.set_val("HB_NBUFS","8"); // NOLINT
    config.set_val("HB_BUFSZ","4096"); // NOLINT

    // data ring buffer defintion
    config.set_val("DB_NBUFS","8"); // NOLINT
    config.set_val("DB_BUFSZ","1048576"); // NOLINT

    // weights ring buffer defintion
    config.set_val("WB_NBUFS","8"); // NOLINT
    config.set_val("WB_BUFSZ","8192"); // NOLINT

    dbm->configure_beam(config); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured()); // NOLINT

    EXPECT_EQ(config.get_val("DATA_KEY"), dbm->get_beam_configuration_header_key("DATA_KEY")); // NOLINT
    EXPECT_EQ(config.get_val("WEIGHTS_KEY"), dbm->get_beam_configuration_header_key("WEIGHTS_KEY")); // NOLINT
    EXPECT_EQ(config.get_val("HB_NBUFS"), std::to_string(dbm->get_beam_configuration_buffer_key("HB_NBUFS"))); // NOLINT
    EXPECT_EQ(config.get_val("HB_BUFSZ"), std::to_string(dbm->get_beam_configuration_buffer_key("HB_BUFSZ"))); // NOLINT
    EXPECT_EQ(config.get_val("DB_NBUFS"), std::to_string(dbm->get_beam_configuration_buffer_key("DB_NBUFS"))); // NOLINT
    EXPECT_EQ(config.get_val("DB_BUFSZ"), std::to_string(dbm->get_beam_configuration_buffer_key("DB_BUFSZ"))); // NOLINT
    EXPECT_EQ(config.get_val("WB_NBUFS"), std::to_string(dbm->get_beam_configuration_buffer_key("WB_NBUFS"))); // NOLINT
    EXPECT_EQ(config.get_val("WB_BUFSZ"), std::to_string(dbm->get_beam_configuration_buffer_key("WB_BUFSZ"))); // NOLINT

    dbm->deconfigure_beam(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    EXPECT_THROW(dbm->get_beam_configuration_header_key("DATA_KEY"), std::runtime_error); // NOLINT
}


TEST_F(DataBlockManagerTest, test_assigned_release_then_reassign_resources) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::test_get_beam_configuration"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
    EXPECT_FALSE(dbm->is_beam_configured()); // NOLINT

    ska::pst::common::AsciiHeader config; // NOLINT
    config.set_val("DATA_KEY","a000"); // NOLINT
    config.set_val("WEIGHTS_KEY","a010"); // NOLINT
    // config.set_val("NUMA_NODE","0"); // NOLINT

    // common header ring buffer defintion
    config.set_val("HB_NBUFS","8"); // NOLINT
    config.set_val("HB_BUFSZ","4096"); // NOLINT

    // data ring buffer defintion
    config.set_val("DB_NBUFS","8"); // NOLINT
    config.set_val("DB_BUFSZ","1048576"); // NOLINT

    // weights ring buffer defintion
    config.set_val("WB_NBUFS","8"); // NOLINT
    config.set_val("WB_BUFSZ","8192"); // NOLINT

    dbm->configure_beam(config); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured()); // NOLINT

    dbm->deconfigure_beam(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
    EXPECT_FALSE(dbm->is_beam_configured()); // NOLINT

    dbm->configure_beam(config); // NOLINT
    ASSERT_EQ(ska::pst::common::State::BeamConfigured, dbm->get_state()); // NOLINT
    EXPECT_TRUE(dbm->is_beam_configured()); // NOLINT

    dbm->deconfigure_beam(); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
    EXPECT_FALSE(dbm->is_beam_configured()); // NOLINT
}

TEST_F(DataBlockManagerTest, reset_when_in_runtime_error_state_returns_to_idle) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::reset_when_in_runtime_error_state_returns_to_idle"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    try {
        throw std::runtime_error("system needs to be in a runtime error state");
    } catch (...) {
        dbm->go_to_runtime_error(std::current_exception()); // NOLINT
    }
    ASSERT_EQ(ska::pst::common::State::RuntimeError, dbm->get_state()); // NOLINT

    dbm->reset();
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
}

TEST_F(DataBlockManagerTest, reset_when_not_in_runtime_error_state) // NOLINT
{
    SPDLOG_TRACE("ska::pst::common::test::DataBlockManagerTest::reset_when_not_in_runtime_error_state"); // NOLINT
    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT

    // clients need to ensure that the state RuntimeError before calling
    ASSERT_THROW(dbm->reset(), ska::pst::common::pst_state_transition_error);

    ASSERT_EQ(ska::pst::common::State::Idle, dbm->get_state()); // NOLINT
}

} // namespace ska::pst::smrb::test