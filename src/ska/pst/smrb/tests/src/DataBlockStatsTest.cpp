/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/tests/DataBlockStatsTest.h"
#include "ska/pst/smrb/DataBlockStats.h"
#include "ska/pst/smrb/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::smrb::test::gtest_main(argc, argv); // NOLINT
}

namespace ska::pst::smrb::test {

DataBlockStatsTest::DataBlockStatsTest()
  : ::testing::Test()
{
}

void DataBlockStatsTest::SetUp()
{
  _db = std::make_shared<DataBlockCreate>("dada"); // NOLINT
  _db->create(hdr_nbufs, hdr_bufsz, data_nbufs, data_bufsz, num_readers, device_id); // NOLINT
}

void DataBlockStatsTest::TearDown()
{
  if (_db)
  {
    _db->destroy(); // NOLINT
  }
  _db = nullptr; // NOLINT
}

TEST_F(DataBlockStatsTest, test_construct_delete) // NOLINT
{
  auto db = reinterpret_cast<DataBlock *>(_db.get()); // NOLINT
  DataBlockStats dbs(*db); // NOLINT
}

TEST_F(DataBlockStatsTest, test_stats_string) // NOLINT
{
  auto db = reinterpret_cast<DataBlock *>(_db.get()); // NOLINT
  DataBlockStats dbs(*db); // NOLINT
  std::string header_str = dbs.get_header_stats_string(); // NOLINT
  std::string expected_header_str = "HEADER nbufs=5 bufsz=8192 written=0 read=0 full=0 clear=0 available=5"; // NOLINT
  EXPECT_EQ(header_str, expected_header_str); // NOLINT
  std::string data_str = dbs.get_data_stats_string(); // NOLINT
  std::string expected_data_str = "DATA   nbufs=6 bufsz=1048576 written=0 read=0 full=0 clear=0 available=6"; // NOLINT
  EXPECT_EQ(data_str, expected_data_str); // NOLINT
}

TEST_F(DataBlockStatsTest, test_print_stats) // NOLINT
{
  auto db = reinterpret_cast<DataBlock *>(_db.get()); // NOLINT
  DataBlockStats dbs(*db); // NOLINT
  dbs.print_stats(); // NOLINT
}

} // namespace ska::pst::smrb::test