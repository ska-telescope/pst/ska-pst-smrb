/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/DataBlockCreate.h"

ska::pst::smrb::DataBlockCreate::DataBlockCreate(const std::string &key_string) : DataBlock(key_string)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockCreate::DataBlockCreate key_string={}", key_string);
}

ska::pst::smrb::DataBlockCreate::~DataBlockCreate()
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockCreate::~DataBlockCreate");
  destroy();
}

void ska::pst::smrb::DataBlockCreate::create(uint64_t hdr_nbufs, uint64_t hdr_bufsz, uint64_t data_nbufs, uint64_t data_bufsz, unsigned num_readers, int device_id)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockCreate::create data nbufs={} bufsz={} nread={}", data_nbufs, data_bufsz, num_readers);
  if (created){
    throw std::runtime_error("ska::pst::smrb::DataBlockCreate::create block already created");
  }
  if (ipcbuf_create_work(reinterpret_cast<ipcbuf_t *>(&data_block), data_block_key, data_nbufs, data_bufsz, num_readers, device_id) < 0)
  {
    SPDLOG_ERROR("ska::pst::smrb::DataBlockCreate::create could not create DADA data block with key={} bufsz={} nbufsz={} nread={}", data_block_key, data_bufsz, data_nbufs, num_readers);
    throw std::runtime_error("ska::pst::smrb::DataBlockCreate::create ipcbuf_create_work failed");
  }

  SPDLOG_DEBUG("ska::pst::smrb::DataBlockCreate::create header nbufs={} bufsz={} nread={}", hdr_nbufs, hdr_bufsz, num_readers);
  if (ipcbuf_create(&header_block, header_block_key, hdr_nbufs, hdr_bufsz, num_readers) < 0)
  {
    SPDLOG_ERROR("ska::pst::smrb::DataBlockCreate::create Could not create DADA header block with key=%x bufsz=%lu, nbufs=%lu", header_block_key, hdr_nbufs, hdr_bufsz);
    throw std::runtime_error("ska::pst::smrb::DataBlockCreate::create ipcbuf_create failed");
  }
  created = true;
  on_connect();
}

void ska::pst::smrb::DataBlockCreate::destroy()
{
  if (created)
  {
    SPDLOG_TRACE("ska::pst::smrb::DataBlockCreate::destory ipcbuf_destroy({})", reinterpret_cast<void *>(&data_block));
    ipcbuf_destroy(reinterpret_cast<ipcbuf_t *>(&data_block));
    SPDLOG_TRACE("ska::pst::smrb::DataBlockCreate::destory ipcbuf_destroy({})", reinterpret_cast<void *>(&header_block));
    ipcbuf_destroy(&header_block);
    created = false;
    on_disconnect();
  }
}