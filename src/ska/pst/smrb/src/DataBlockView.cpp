/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/config.h"
#include "ska/pst/smrb/DataBlockView.h"
#include "ska/pst/common/utils/AsciiHeader.h"

ska::pst::smrb::DataBlockView::DataBlockView(const std::string &key_string) : ska::pst::smrb::DataBlockAccessor(key_string)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::DataBlockView key_string={}", key_string);
}

ska::pst::smrb::DataBlockView::~DataBlockView()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::~DataBlockView()");
}

void ska::pst::smrb::DataBlockView::open()
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::open()");
  check_opened(false);

  if (ipcio_open(&data_block, 'r') < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockView::open could not open data block for viewing");
  }
  opened = true;
}

void ska::pst::smrb::DataBlockView::close()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::close()");
  check_opened(true);
  opened = false;
  clear_header();
}

void ska::pst::smrb::DataBlockView::lock()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::lock()");
  check_connected(true);
  locked = true;
}

void ska::pst::smrb::DataBlockView::unlock()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::unlock()");
  locked = false;
}

auto ska::pst::smrb::DataBlockView::open_block() -> char *
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::open_block()");
  check_opened(true);

  ipcbuf_t * buf = &(data_block.buf);
  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::open_block write_buf={}", ipcbuf_get_write_count(buf));
  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::open_block viewbuf={}", buf->viewbuf);
  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::open_block call ipcio_open_block_read");

  curr_buf = ipcio_open_block_read(&data_block, &curr_buf_bytes, &curr_buf_id);

  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::open_block write_buf={}", ipcbuf_get_write_count(buf));
  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::open_block viewbuf={}", buf->viewbuf);

  return curr_buf;
}

auto ska::pst::smrb::DataBlockView::close_block(uint64_t bytes) -> ssize_t
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::close_block({})", bytes);
  check_opened(true);

  data_block.bytes = 0;
  data_block.curbuf = nullptr;

  return static_cast<ssize_t>(bytes);
}

auto ska::pst::smrb::DataBlockView::read_data(char * buffer, size_t bytes) -> ssize_t
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_data({}, {})", reinterpret_cast<void *>(buffer), bytes);
  check_opened(true);

  return ipcio_read(&data_block, buffer, bytes);
}

auto ska::pst::smrb::DataBlockView::get_current_buffer_index() const -> size_t
{
  const ipcbuf_t* buf = &(data_block.buf);
  return buf->viewbuf;
}

auto ska::pst::smrb::DataBlockView::seek_to_end(uint64_t byte_resolution) -> int
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::seek_to_send({})", byte_resolution);
  check_opened(true);

  ipcbuf_t* buf = &(data_block.buf);

  if (ipcbuf_eod(buf))
  {
    SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_end end of data");
    data_block.bytes = 0;
    data_block.curbuf = nullptr;
    return 0;
  }

  size_t writebuf = ipcbuf_get_write_count(buf);

  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_end writebuf={}", writebuf);
  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_end incrementing viewbuf={}", buf->viewbuf);

  buf->viewbuf ++;

  if (writebuf > buf->viewbuf + 1)
  {
    buf->viewbuf = writebuf - 1;
    SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_end viewbuf=write_buf-1={}", buf->viewbuf);
  }

  data_block.bytes = 0;
  data_block.curbuf = nullptr;

  if (byte_resolution)
  {
    // find last byte written in the data block
    SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_end calling ipcio_tell on data_block");
    uint64_t current = ipcio_tell(&data_block);
    uint64_t too_far = current % byte_resolution;
    SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_end too_far={}", too_far);
    if (too_far)
    {
      int64_t absolute_bytes = seek(current + byte_resolution - too_far, SEEK_SET); // NOLINT
      if (absolute_bytes < 0)
      {
        return -1;
      }
    }
  }

  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_end exiting");
  return 0;
}

void ska::pst::smrb::DataBlockView::seek_to_buffer_index(size_t buffer_index)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::seek_to_buffer_index {}", buffer_index);

  if (buffer_index == 0)
  {
    const char* msg = "ska::pst::smrb::DataBlockView::seek_to_buffer_index cannot seek to buffer_index=0";
    SPDLOG_TRACE(msg);
    throw std::runtime_error(msg);
  }

  check_opened(true);

  ipcbuf_t * buf = &(data_block.buf);

  if (ipcbuf_eod(buf))
  {
    SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_buffer_index end of data");
    data_block.bytes = 0;
    data_block.curbuf = nullptr;
    return;
  }

  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_buffer_index current writer block_index={}", ipcbuf_get_write_count(buf));

  buf->viewbuf = buffer_index - 1; // the next call to ipcio_open_block_read will increment this
  data_block.bytes = 0;
  data_block.curbuf = nullptr;

  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::seek_to_buffer_index exiting");
}

auto ska::pst::smrb::DataBlockView::seek(int64_t offset, int whence) -> ssize_t
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::seek({}, {})", offset, whence);

  check_opened(true);

  return ipcio_seek(&data_block, offset, whence);
}

void ska::pst::smrb::DataBlockView::read_config()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_config()");
  check_have_config(false);

  // read_header_buffer will read the most recently written header
  read_header_buffer(config);

  // check if the UTC_START and SCAN_ID are present, if so the full header has been written
  ska::pst::common::AsciiHeader received;
  received.load_from_string(std::string(config.begin(), config.end()));
  if (received.has("UTC_START") && received.has("SCAN_ID"))
  {
    SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_config received contained UTC_START and SCAN_ID");
    memcpy(&header[0], &config[0], header_bufsz);
    have_header = true;
  }

  have_config = true;
}

void ska::pst::smrb::DataBlockView::read_header()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_header()");
  check_have_config(true);

  if (!have_header)
  {
    SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_header calling read_header_buffer()");
    read_header_buffer(header);
    have_header = true;
  }
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_header returned");
}

void ska::pst::smrb::DataBlockView::read_header_buffer(std::vector<char>& local_buffer)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_header_buffer()");

  uint64_t bytes = 0;
  auto header_ptr = ipcbuf_get_next_read(&header_block, &bytes);
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_header_buffer ipcbuf_get_next_read returned {} bytes", bytes);

  while (!bytes)
  {
    // wait for the next header block
    header_ptr = ipcbuf_get_next_read(&header_block, &bytes);
    SPDLOG_DEBUG("ska::pst::smrb::DataBlockView::read_header_buffer ipcbuf_get_next_read returned {} bytes", bytes);

    // the view doesn't mark the header buffer as cleared
    if (ipcbuf_eod(&(header_block)))
    {
      SPDLOG_TRACE("ska::pst::smrb::DataBlockView::read_header_buffer end of data on header block");
      if (ipcbuf_is_reader(&header_block))
      {
        ipcbuf_reset(&header_block);
      }
    }
    else
    {
      throw std::runtime_error("ska::pst::smrb::DataBlockView::read_header empty header block");
    }
  }

  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::read_header copying header_ptr to this->header");
  memcpy(&local_buffer[0], header_ptr, header_bufsz);
}

auto ska::pst::smrb::DataBlockView::get_curr_buf() -> char *
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockView::get_curr_buf()");

  check_opened(true);

  return data_block.curbuf;
}
