/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>
#include <regex>

#include "ska/pst/smrb/DataBlockManager.h"
#include "ska/pst/common/utils/ValidationContext.h"

void check_header_key(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext* context, std::string _val_expected_key, std::string _key_pattern)
{
  std::string val_expected_key(std::move(_val_expected_key));
  std::string key_pattern(std::move(_key_pattern));

  if (config.has(val_expected_key)) {
    auto config_value = config.get_val(val_expected_key);
    std::regex regex_pattern(key_pattern);
    if (!std::regex_match(config_value, regex_pattern))
    {
      context->add_value_regex_error(val_expected_key, config_value, key_pattern);
    }
  } else {
    context->add_missing_field_error(val_expected_key);
  }
}

void ska::pst::smrb::DataBlockManager::validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::validate_configure_beam");

  // Loop through header keys
  for (const auto &val_expected_key : header_keys)
  {
    check_header_key(config, context, val_expected_key, key_pattern);
  }

  for (const auto &val_expected_key : buffer_keys)
  {
    check_header_key(config, context, val_expected_key, buffer_pattern);
  }

  context->throw_error_if_not_empty();
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::validate_configure_beam - is valid");
}

void ska::pst::smrb::DataBlockManager::validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context) // NOLINT
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::validate_configure_scan");
}

void ska::pst::smrb::DataBlockManager::validate_start_scan(const ska::pst::common::AsciiHeader& config) // NOLINT
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::validate_start_scan");
}

void ska::pst::smrb::DataBlockManager::perform_initialise()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_initialise");
}

void ska::pst::smrb::DataBlockManager::perform_configure_beam()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_configure_beam");
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure data_key={}", beam_config.get_val("DATA_KEY"));
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure weights_key={}", beam_config.get_val("WEIGHTS_KEY"));
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure hb_nbufs={}", beam_config.get_uint64("HB_NBUFS"));
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure hb_bufsz={}", beam_config.get_uint64("HB_BUFSZ"));
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure db_nbufs={}", beam_config.get_uint64("DB_NBUFS"));
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure db_bufsz={}", beam_config.get_uint64("DB_BUFSZ"));
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure wb_nbufs={}", beam_config.get_uint64("WB_NBUFS"));
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::configure wb_bufsz={}", beam_config.get_uint64("WB_BUFSZ"));

  check_beam_configured(false);

  SPDLOG_INFO("Initializing data and weights ring buffers");
  data_ring_buffer = std::make_shared<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("DATA_KEY"));
  weights_ring_buffer = std::make_shared<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("WEIGHTS_KEY"));

  SPDLOG_INFO("Initializing data and weights stats");
  data_ring_stats = std::make_shared<ska::pst::smrb::DataBlockStats>(*data_ring_buffer);
  weights_ring_stats = std::make_shared<ska::pst::smrb::DataBlockStats>(*weights_ring_buffer);

  SPDLOG_INFO("Creating data and weights ring buffers");
  data_ring_buffer->create(beam_config.get_uint64("HB_NBUFS"), beam_config.get_uint64("HB_BUFSZ"), beam_config.get_uint64("DB_NBUFS"), beam_config.get_uint64("DB_BUFSZ"), num_readers, device_id);
  weights_ring_buffer->create(beam_config.get_uint64("HB_NBUFS"), beam_config.get_uint64("HB_BUFSZ"), beam_config.get_uint64("WB_NBUFS"), beam_config.get_uint64("WB_BUFSZ"), num_readers, device_id);

  beam_configured = true;
}

void ska::pst::smrb::DataBlockManager::perform_configure_scan()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_configure_scan");
}

void ska::pst::smrb::DataBlockManager::perform_start_scan()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_start_scan");
}

void ska::pst::smrb::DataBlockManager::perform_scan()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_scan");
}

void ska::pst::smrb::DataBlockManager::perform_stop_scan()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_stop_scan");
}

void ska::pst::smrb::DataBlockManager::perform_deconfigure_scan()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_deconfigure_scan");
}

void ska::pst::smrb::DataBlockManager::perform_deconfigure_beam()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_deconfigure_beam");
  check_beam_configured(true);

  SPDLOG_INFO("Releasing data and weights ring buffers");
  data_ring_buffer->destroy();
  weights_ring_buffer->destroy();
  beam_configured = false;

  beam_config.reset();

  data_ring_buffer = nullptr;
  data_ring_stats = nullptr;
  weights_ring_buffer = nullptr;
  weights_ring_stats = nullptr;
}

void ska::pst::smrb::DataBlockManager::perform_terminate()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::perform_terminate");
}

void ska::pst::smrb::DataBlockManager::configure_beam_from_file(const std::string &config_file)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockManager::configure_beam_from_file file={}", config_file);
  ska::pst::common::AsciiHeader config;
  config.load_from_file(config_file);
  SPDLOG_TRACE("ska::pst::smrb::DataBlockManager::configure_beam_from_file done", config_file);

  SPDLOG_TRACE("ska::pst::smrb::DataBlockManager::configure_beam");
  configure_beam(config);
  SPDLOG_TRACE("ska::pst::smrb::DataBlockManager::configure_beam done");
}

auto ska::pst::smrb::DataBlockManager::get_beam_configuration_header_key(const std::string &header_key) -> std::string
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::get_beam_configuration_header_key header_key={}", header_key);
  check_beam_configured(true);
  return beam_config.get_val(header_key);
}

auto ska::pst::smrb::DataBlockManager::get_beam_configuration_buffer_key(const std::string &buffer_key) -> uint64_t
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::get_beam_configuration_buffer_key buffer_key={}", buffer_key);
  check_beam_configured(true);
  return beam_config.get_uint64(buffer_key);
}

void ska::pst::smrb::DataBlockManager::print_statistics()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::print_statistics()");
  check_beam_configured(true);
  data_ring_stats->print_stats();
  weights_ring_stats->print_stats();
}

auto ska::pst::smrb::DataBlockManager::get_data_header_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return data_ring_stats->get_header_stats();
}

auto ska::pst::smrb::DataBlockManager::get_data_data_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return data_ring_stats->get_data_stats();
}

auto ska::pst::smrb::DataBlockManager::get_weights_header_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return weights_ring_stats->get_header_stats();
}

auto ska::pst::smrb::DataBlockManager::get_weights_data_stats() -> ska::pst::smrb::DataBlockStats::stats_t
{
  check_beam_configured(true);
  return weights_ring_stats->get_data_stats();
}

void ska::pst::smrb::DataBlockManager::check_beam_configured(bool required)
{
  if (beam_configured != required)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockManager::check_beam_configured configured != required");
  }
}

void ska::pst::smrb::DataBlockManager::wait_for_duration(int duration_ms, volatile bool * signal)
{
  check_beam_configured(true);
  static constexpr int sleep_duration_ms = 10;
  static constexpr int microseconds_per_millisecond = 1000;
  bool persist = true;
  int remaining = duration_ms;
  while (persist)
  {
    usleep(sleep_duration_ms * microseconds_per_millisecond);
    remaining -= sleep_duration_ms;
    persist = ((remaining > 0) && (!(*signal)));
  }
}

void ska::pst::smrb::DataBlockManager::wait_for_observation(volatile bool * signal)
{
  check_beam_configured(true);

  static constexpr unsigned sleep_duration_us = 100000;
  bool persist = true;
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::wait_for_observation waiting for single observation to complete");
  while (persist)
  {
    usleep(sleep_duration_us);
    bool observation_complete =
      (data_ring_buffer->get_data_write_xfer() == 1) &&
      (data_ring_buffer->get_data_read_xfer() == 1) &&
      (data_ring_buffer->get_writer_state() == State::DISCONNECTED) &&
      (data_ring_buffer->get_reader_state() == State::DISCONNECTED);

    SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::wait_for_observation write_xfer={} read_xfer={} writer_state={} reader_state={}",
      data_ring_buffer->get_data_write_xfer(), data_ring_buffer->get_data_read_xfer(), data_ring_buffer->get_writer_state(), data_ring_buffer->get_reader_state());
    persist = (!*signal && !observation_complete);
  }
  static constexpr unsigned second_in_microseconds = 1000000;
  usleep(second_in_microseconds);
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockManager::wait_for_observation exiting");
}

void ska::pst::smrb::DataBlockManager::wait_for_signal(volatile bool * signal)
{
  static constexpr unsigned sleep_duration_us = 100000;
  bool persist = true;
  while (persist)
  {
    usleep(sleep_duration_us);
    persist = !(*signal);
  }
}
