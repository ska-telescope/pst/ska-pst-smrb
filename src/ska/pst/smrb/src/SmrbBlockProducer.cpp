/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "ska/pst/smrb/SmrbBlockProducer.h"

#include <spdlog/spdlog.h>
#include <sys/mman.h>

ska::pst::smrb::SmrbBlockProducer::SmrbBlockProducer(const std::string &key)
{
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer ctor instantiating DataBlockView({})", key);
  viewer = std::make_unique<ska::pst::smrb::DataBlockView>(key);

  void *ptr = current_block.block;
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer ctor DataBlockView constructed current block ptr={}", ptr);
}

void ska::pst::smrb::SmrbBlockProducer::connect(int timeout)
{
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::connect viewer->connect({})", timeout);
  viewer->connect(timeout);

  if (viewer->is_connected())
  {
    SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::connect viewer->lock()");
    viewer->lock();
  }

  if (viewer->get_locked())
  {
    viewer->read_config();
    SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::connect header.load_from_str");
    header.load_from_str(viewer->get_config());
    SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::connect header={}", header.raw());
  }
}

void ska::pst::smrb::SmrbBlockProducer::open()
{
  // header has the UTC_START following the start of the scan
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::open viewer->read_header()");
  viewer->read_header();
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::open header.load_from_str");
  header.load_from_str(viewer->get_header());
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::open header={}", header.raw());
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::open viewer->open()");
  viewer->open();
}

void ska::pst::smrb::SmrbBlockProducer::close()
{
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::close");
  if (viewer->get_opened())
  {
    SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::close viewer->close()");
    viewer->close();
  }
}

void ska::pst::smrb::SmrbBlockProducer::disconnect()
{
  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::disconnect");

  if (viewer->get_locked())
  {
    SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::disconnect viewer->unlock()");
    viewer->unlock();
  }

  if (viewer->is_connected())
  {
    SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::disconnect viewer->disconnect()");
    viewer->disconnect();
  }
}

ska::pst::smrb::SmrbBlockProducer::~SmrbBlockProducer()
{
  if (viewer)
  {
    close();
    disconnect();
    viewer = nullptr;
  }
}

auto ska::pst::smrb::SmrbBlockProducer::get_header() const -> const ska::pst::common::AsciiHeader&
{
  return header;
}

auto ska::pst::smrb::SmrbBlockProducer::next_block() -> ska::pst::common::BlockProducer::Block
{
  if (seek_to_end)
  {
    SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::next_block viewer->seek_to_end()");
    viewer->seek_to_end();
  }

  SPDLOG_TRACE("ska::pst::smrb::SmrbBlockProducer::next_block viewer->open_block()");
  current_block.block = viewer->open_block();
  current_block.size = viewer->get_buf_bytes();
  current_block.obs_offset = viewer->get_current_buffer_index() * viewer->get_data_bufsz();

  // on next call to next_block, call seek_to_end unless seek_to_buffer_index is called first
  seek_to_end = true;

  return current_block;
}

auto ska::pst::smrb::SmrbBlockProducer::get_current_buffer_index() const -> size_t
{
  return viewer->get_current_buffer_index();
}

void ska::pst::smrb::SmrbBlockProducer::seek_to_buffer_index(size_t buffer_index)
{
  viewer->seek_to_buffer_index(buffer_index);
  seek_to_end = false;
}