/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/config.h"
#include "ska/pst/smrb/DataBlockWrite.h"

ska::pst::smrb::DataBlockWrite::DataBlockWrite(const std::string &key_string) : ska::pst::smrb::DataBlockAccessor(key_string)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::DataBlockWrite key_string={}", key_string);
}

ska::pst::smrb::DataBlockWrite::~DataBlockWrite()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::~DataBlockWrite()");
}

void ska::pst::smrb::DataBlockWrite::open()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::open()");
  check_have_header(true);
  check_opened(false);

  if (ipcio_open(&data_block, 'W') < 0)
  {
    throw std::runtime_error("could not open data block for writing");
  }
  opened = true;
}

void ska::pst::smrb::DataBlockWrite::lock()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::lock()");
  check_connected(true);
  check_locked(false);

  if (ipcbuf_lock_write(&header_block) < 0)
  {
    throw std::runtime_error("could not lock header block for writing");
  }
  locked = true;
}

void ska::pst::smrb::DataBlockWrite::close()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::close()");
  check_opened(true);

  if (ipcio_is_open(&data_block))
  {
    SPDLOG_TRACE("ska::pst::smrb::DataBlockWrite::close ipcio_close()");
    if (ipcio_close(&data_block) < 0)
    {
      throw std::runtime_error("could not unlock data block from writing");
    }
  }

  clear_header();
  opened = false;
}

void ska::pst::smrb::DataBlockWrite::unlock()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::unlock()");
  check_locked(true);

  // if we are unlocking, we must close first
  if (opened)
  {
    close();
  }

  if (ipcbuf_unlock_write(&header_block) < 0)
  {
    throw std::runtime_error("could not unlock header block from writing");
  }

  locked = false;
}

void ska::pst::smrb::DataBlockWrite::write_config(const std::string& scan_config)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::write_config()");
  check_locked(true);
  check_have_config(false);

  write_header_buffer(scan_config);

  have_config = true;
}

void ska::pst::smrb::DataBlockWrite::write_header(const std::string& full_header)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockWrite::write_header()");
  check_have_config(true);
  check_have_header(false);

  write_header_buffer(full_header);

  have_header = true;
}

void ska::pst::smrb::DataBlockWrite::write_header_buffer(const std::string& str)
{
  char * header_buf = ipcbuf_get_next_write(&header_block);
  if (header_buf == nullptr)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockWrite::write_header_buffer: could not get next header buffer");
  }

  size_t to_copy = std::min(str.size(), header_bufsz - 1);

  // ensure the header is initialized with the null character string
  memset(header_buf, '\0', header_bufsz);

  // copy the header to the initialized buffer
  memcpy(header_buf, str.c_str(), to_copy);

  if (ipcbuf_mark_filled(&header_block, header_bufsz) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockWrite::write_header_buffer: could not mark header buffer filled");
  }
}

auto ska::pst::smrb::DataBlockWrite::write_data(char * buffer, size_t bytes) -> ssize_t
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockWrite::write_data writing {} bytes to {}", bytes, reinterpret_cast<void *>(buffer));
  check_opened(true);

  ssize_t bytes_written = ipcio_write(&data_block, buffer, bytes);
  if (bytes_written < 0)
  {
    throw std::runtime_error("could not write bytes to data block");
  }

  return bytes_written;
}

auto ska::pst::smrb::DataBlockWrite::open_block() -> char *
{
  check_opened(true);

  curr_buf = ipcio_open_block_write(&data_block, &curr_buf_id);
  opened_blocks.push(curr_buf);

  block_open = true;
  return curr_buf;
}

auto ska::pst::smrb::DataBlockWrite::close_block(uint64_t bytes) -> ssize_t
{
  check_opened(true);

  opened_blocks.pop();
  block_open = (opened_blocks.size() > 0);
  if (block_open)
  {
    curr_buf = opened_blocks.front();
  }

  return ipcio_close_block_write(&data_block, bytes);
}

void ska::pst::smrb::DataBlockWrite::zero_next_block()
{
  check_opened(true);

  // requires data block reader for this function to run
  //if (ipcio_zero_next_block(&data_block) < 0)
  //{
  //  throw std::runtime_error("could not zero next block");
  //}
}
