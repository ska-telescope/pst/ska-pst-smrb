/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/config.h"
#include "ska/pst/smrb/DataBlockRead.h"

ska::pst::smrb::DataBlockRead::DataBlockRead(const std::string &key_string) : ska::pst::smrb::DataBlockAccessor(key_string)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::DataBlockRead key_string={}", key_string);
}

ska::pst::smrb::DataBlockRead::~DataBlockRead()
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::~DataBlockRead()");
}

void ska::pst::smrb::DataBlockRead::open()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::open()");
  check_have_header(true);
  check_opened(false);

  if (ipcio_open(&data_block, 'R') < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::open could not open data_block for reading");
  }
  opened = true;
}

void ska::pst::smrb::DataBlockRead::lock()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::lock()");
  check_connected(true);
  check_locked(false);

  if (ipcbuf_lock_read(&header_block) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::lock could not lock header block for reading");
  }
  locked = true;
}

void ska::pst::smrb::DataBlockRead::unlock()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::unlock()");
  check_locked(true);
  check_opened(false);

  if (ipcbuf_unlock_read(&header_block) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::unlock could not unlock header block from reading");
  }
  locked = false;
}

void ska::pst::smrb::DataBlockRead::close()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::close()");
  check_opened(true);

  if (ipcio_is_open(&data_block))
  {
    SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::close ipcio_close()");
    if (ipcio_close(&data_block) < 0)
    {
      throw std::runtime_error("ska::pst::smrb::DataBlockRead::close could not unlock data block from reading");
    }
  }
  opened = false;
  clear_header();
}

void ska::pst::smrb::DataBlockRead::read_config()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::read_config()");
  check_locked(true);
  check_have_config(false);

  uint64_t bytes{};
  char * buf = ipcbuf_get_next_read(&header_block, &bytes);

  // make a local copy of the config
  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::read_header copying header_buf to config");
  memcpy(&config[0], buf, header_bufsz);

  if (ipcbuf_mark_cleared(&header_block) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::read_config could not mark header buffer cleared");
  }
  have_config = true;
}

void ska::pst::smrb::DataBlockRead::read_header()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::read_header()");
  check_have_config(true);
  check_have_header(false);

  uint64_t bytes{};
  char * buf = ipcbuf_get_next_read(&header_block, &bytes);
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::read_header read {} bytes", bytes);

  // make a local copy of the header
  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::read_header copying header_buf to header");
  memcpy(&header[0], buf, header_bufsz);

  if (ipcbuf_mark_cleared(&header_block) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::read_header could not mark header buffer cleared");
  }
  have_header = true;
}

auto ska::pst::smrb::DataBlockRead::read_data(char * buffer, size_t bytes) -> ssize_t
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::read_data buffer={} bytes={}", reinterpret_cast<void*>(buffer), bytes);
  check_opened(true);

  if (check_eod())
  {
    SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::read_data EoD true");
    return 0;
  }

  ssize_t bytes_read = ipcio_read(&data_block, buffer, bytes);
  
  if (bytes_read < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::read_data could not read bytes from data block");
  }
  return bytes_read;
}

auto ska::pst::smrb::DataBlockRead::open_block() -> char *
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::open_block()");
  check_opened(true);

  if (block_open)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::open_block block is already open");
  }

  // check for EOD prior to opening a new block
  if (check_eod())
  {
    curr_buf_bytes = 0;
    curr_buf = nullptr;
    SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::open_block EOD encountered, returning nullptr");
    return curr_buf;
  }

  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::open_block ipcio_open_block_read()");
  curr_buf = ipcio_open_block_read(&data_block, &curr_buf_bytes ,&curr_buf_id);
  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::open_block curr_buf_bytes={} curr_buf_id={} curr_buf={}", curr_buf_bytes, curr_buf_id, reinterpret_cast<void*>(curr_buf));

  if (curr_buf_bytes == 0)
  {
    SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::open_block ipcio_open_block_read returned 0 bytes, eod={}", check_eod());
  }

  if (curr_buf == nullptr)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::open_block failed to open block for reading");
  }

  block_open = true;
  return curr_buf;
}

auto ska::pst::smrb::DataBlockRead::close_block(uint64_t bytes) -> ssize_t
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlockRead::close_block bytes={}", bytes);
  check_opened(true);

  if (!block_open) {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::close_block block already closed");
  }

  if (bytes != curr_buf_bytes)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::close_block bytes != curr_buf_bytes");
  }

  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::close_block ipcio_close_block_read(&data_block, {})", bytes);
  ssize_t result = ipcio_close_block_read(&data_block, bytes);

  curr_buf = nullptr;
  curr_buf_bytes = 0;
  block_open = false;

  return result;
}

auto ska::pst::smrb::DataBlockRead::check_eod() -> bool
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockRead::check_eod()");
  check_opened(true);

  int eod = ipcbuf_eod(&data_block.buf);
  if (eod < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlockRead::check_eod ipcbuf_eod failed");
  }

  return (eod > 0);
}
