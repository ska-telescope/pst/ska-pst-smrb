/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/config.h"
#include "ska/pst/smrb/DataBlockAccessor.h"

ska::pst::smrb::DataBlockAccessor::DataBlockAccessor(const std::string &key_string) : ska::pst::smrb::DataBlock(key_string)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockAccessor::DataBlockAccessor key_string={}", key_string);
}

void ska::pst::smrb::DataBlockAccessor::connect(int timeout)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockAccessor::connect timeout={} ", timeout);
  ska::pst::smrb::DataBlock::connect(timeout);
  SPDLOG_TRACE("ska::pst::smrb::DataBlockAccessor::connect header.resize({})", header_bufsz);
  config.resize(header_bufsz);
  header.resize(header_bufsz);
}

void ska::pst::smrb::DataBlockAccessor::disconnect()
{
  locked = false;
  block_open = false;
  curr_buf = nullptr;
  curr_buf_bytes = 0;
  curr_buf_id = 0;
  config.resize(0);
  header.resize(0);
  ska::pst::smrb::DataBlock::disconnect();
}

void ska::pst::smrb::DataBlockAccessor::check_locked(bool expected)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockAccessor::check_locked expected={}", expected);
  if (expected)
  {
    check_connected(true);
  }
  if (locked != expected)
  {
    SPDLOG_ERROR("ska::pst::smrb::DataBlockAccessor::check_locked locked={} did not equal expected={}", locked, expected);
    throw std::runtime_error("ska::pst::smrb::DataBlockAccessor::check_locked locked != expected");
  }
}

void ska::pst::smrb::DataBlockAccessor::check_opened(bool expected)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockAccessor::check_opened expected={}", expected);
  if (expected)
  {
    check_have_header(true);
  }

  if (opened != expected)
  {
    SPDLOG_ERROR("ska::pst::smrb::DataBlockAccessor::check_opened opened={} did not equal expected={}", opened, expected);
    throw std::runtime_error("ska::pst::smrb::DataBlockAccessor::check_opened opened != expected");
  }
}

void ska::pst::smrb::DataBlockAccessor::check_have_config(bool expected)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockAccessor::check_have_config expected={}", expected);
  if (expected)
  {
    check_locked(true);
  }
  if (have_config != expected)
  {
    SPDLOG_ERROR("ska::pst::smrb::DataBlockAccessor::check_have_config have_config={} did not equal expected={}", have_config, expected);
    throw std::runtime_error("ska::pst::smrb::DataBlockAccessor::check_have_config have_config != expected");
  }
}

void ska::pst::smrb::DataBlockAccessor::check_have_header(bool expected)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlockAccessor::check_have_header expected={}", expected);
  if (expected)
  {
    check_have_config(true);
  }
  if (have_header != expected)
  {
    SPDLOG_ERROR("ska::pst::smrb::DataBlockAccessor::check_have_header have_header={} did not equal expected={}", have_header, expected);
    throw std::runtime_error("ska::pst::smrb::DataBlockAccessor::check_have_header have_header != expected");
  }
}

void ska::pst::smrb::DataBlockAccessor::clear_header()
{
  header.clear();
  have_header = false;
}

void ska::pst::smrb::DataBlockAccessor::clear_config()
{
  clear_header();

  config.clear();
  have_config = false;
}

