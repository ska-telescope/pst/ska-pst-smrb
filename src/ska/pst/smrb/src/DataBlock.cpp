/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/DataBlock.h"

#include "ska/pst/smrb/config.h"
#ifdef HAVE_CUDA
#include <cuda_runtime.h>
#endif

ska::pst::smrb::DataBlock::DataBlock(const std::string &key_string) :
  header_block(IPCBUF_INIT),
  data_block(IPCIO_INIT)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlock::DataBlock parsing key_string={}", key_string);
  key_t key = parse_psrdada_key(key_string);

  // keys for the header + data unit
  data_block_key = key;
  header_block_key = key + 1;
  SPDLOG_TRACE("ska::pst::smrb::DataBlock::DataBlock data_block_key={} header_block_key={}",
    data_block_key, header_block_key);
}

void ska::pst::smrb::DataBlock::connect(int timeout)
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlock::connect timeout={}", timeout);
  if (connected)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlock::connect already connected to data block");
  }

  int connection_attempts = 0;
  while (!connected)
  {
    try
    {
      if (ipcbuf_connect(&header_block, header_block_key) < 0)
      {
        throw std::runtime_error("ska::pst::smrb::DataBlock::connect failed to connect to header block");
      }

      if (ipcio_connect(&data_block, data_block_key) < 0)
      {
        throw std::runtime_error("ska::pst::smrb::DataBlock::connect failed to connect to data block");
      }

      on_connect();
    } 
    catch (std::exception& error)
    {
      if (connection_attempts < timeout)
      {
        sleep(1);
        connection_attempts++;
      }
      else
      {
        SPDLOG_WARN("ska::pst::smrb::DataBlock::connect Could not connect to data block after {} attempts", timeout);
        throw std::runtime_error("failed to connect to data block");
      }
    }
  }
}

void ska::pst::smrb::DataBlock::on_connect()
{
  // need to assert that header and data block aren't nullptr;
  SPDLOG_TRACE("ska::pst::smrb::DataBlock::on_connect()");

  header_bufsz = ipcbuf_get_bufsz(&header_block);
  header_nbufs = ipcbuf_get_nbufs(&header_block);
  data_bufsz = ipcbuf_get_bufsz(reinterpret_cast<ipcbuf_t *>(&data_block));
  data_nbufs = ipcbuf_get_nbufs(reinterpret_cast<ipcbuf_t *>(&data_block));

  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  device_id = ipcbuf_get_device(db);
  nreaders = ipcbuf_get_nreaders(db);

  connected = true;
}

void ska::pst::smrb::DataBlock::on_disconnect()
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlock::on_disconnect()");
  connected = false;
  header_bufsz = 0;
  header_nbufs = 0;
  data_bufsz = 0;
  data_nbufs = 0;
  device_id = -1;
  nreaders = 0;
}

void ska::pst::smrb::DataBlock::disconnect()
{
  if (!connected)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlock::disconnect not connected to data block");
  }
  if (ipcio_disconnect(&data_block) < 0) 
  {
    throw std::runtime_error("ska::pst::smrb::DataBlock::disconnect failed to disconnect from data block");
  }
  if (ipcbuf_disconnect(&header_block) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlock::disconnect failed to disconnect from header block");
  }
  on_disconnect();
}

void ska::pst::smrb::DataBlock::check_connected(bool expected)
{
  SPDLOG_TRACE("ska::pst::smrb::DataBlock::check_connected expected={}", expected);
  if (connected != expected)
  {
    SPDLOG_ERROR("ska::pst::smrb::DataBlock::check_connected connected={} did not equal expected={}", connected, expected);
    throw std::runtime_error("ska::pst::smrb::DataBlock::check_connected connected != expected");
  }
}

void ska::pst::smrb::DataBlock::page()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlock::page()");
  check_connected(true);

  if (ipcbuf_page(reinterpret_cast<ipcbuf_t*>(&data_block)) < 0)
  {
    throw std::runtime_error("could not page in data block buffers");
  }
}

auto ska::pst::smrb::DataBlock::get_nreaders() -> int
{
  return nreaders;
}

void ska::pst::smrb::DataBlock::lock_memory()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlock::lock_memory()");
  check_connected(true);

  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  if (ipcbuf_lock(db) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlock::lock_memory failed to lock data buffers into RAM");
  }
}

void ska::pst::smrb::DataBlock::unlock_memory()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlock::unlock_memory()");
  check_connected(true);

  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  if (ipcbuf_unlock(db) < 0)
  {
    throw std::runtime_error("ska::pst::smrb::DataBlock::unlock_memory failed to unlock data buffers from RAM");
  }
}

auto ska::pst::smrb::DataBlock::parse_psrdada_key(const std::string &key_string) -> key_t
{
  if (key_string.length() != 4)
  {
    SPDLOG_WARN("ska::pst::smrb::DataBlock::parse_psrdada_key key={} was not 4 characters in length", key_string);
    throw std::runtime_error("ska::pst::smrb::DataBlock::parse_psrdada_key invalid key");
  }

  std::stringstream ss;
  key_t key = 0;
  ss << std::hex << key_string;
  ss >> key >> std::dec;

  if (!ss.eof() || ss.fail())
  {
    SPDLOG_WARN("ska::pst::smrb::DataBlock::parse_psrdada_key did not parse all characters as hexidecimal from {}", key_string);
    throw std::runtime_error("ska::pst::smrb::DataBlock::parse_psrdada_key invalid key");
  }

  return key;
}

void ska::pst::smrb::DataBlock::register_cuda()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlock::register_cuda()");
  check_connected(true);

  // do not register buffers if they reside on the GPU device
  if (device_id >= 0)
  {
    SPDLOG_DEBUG("ska::pst::smrb::DataBlockAccessor::register_cuda cannot register device memory");
    return;
  }

  #ifdef HAVE_CUDA
  unsigned int flags = 0;
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  for (uint64_t ibuf = 0; ibuf < data_block->sync->nbufs; ibuf++)
  {
    void * buf = reinterpret_cast<void *>(db->buffer[ibuf]);
    cudaError_t rval = cudaHostRegister(buf, data_bufsz, flags);
    if (rval != cudaSuccess)
    {
      SPDLOG_ERROR("ska::pst::smrb::DataBlock::register_cuda cudaHostRegister failed: {}", cudaGetErrorString(rval));
      throw std::runtime_error("could not register host memory with the CUDA driver");
    }
  }
  #endif
}

void ska::pst::smrb::DataBlock::unregister_cuda()
{
  SPDLOG_DEBUG("ska::pst::smrb::DataBlock::unregister_cuda()");
  check_connected(true);

  // dont unregister buffers if they reside on the GPU device
  if (device_id >= 0)
  {
    SPDLOG_DEBUG("ska::pst::smrb::DataBlock::unregister_cuda cannot unregister device memory");
    return;
  }

  #ifdef HAVE_CUDA
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  for (uint64_t ibuf = 0; ibuf < data_block->sync->nbufs; ibuf++)
  {
    void * buf = reinterpret_cast<void *>(db->buffer[ibuf]);
    cudaError_t rval = cudaHostUnregister(buf);
    if (rval != cudaSuccess)
    {
      SPDLOG_ERROR("ska::pst::smrb::DataBlock::unregister_cuda cudaHostUnregister failed: {}", cudaGetErrorString(rval));
      throw std::runtime_error("could not unregister host memory with the CUDA driver");
    }
  }
  #endif
}

auto ska::pst::smrb::DataBlock::get_header_bufs_written() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_write_count(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_written() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_write_count(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_read() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_read_count(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_read() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_read_count(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_clear() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_nclear(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_clear() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_nclear(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_full() -> uint64_t
{
  check_connected(true);
  return ipcbuf_get_nfull(&header_block);
}

auto ska::pst::smrb::DataBlock::get_data_bufs_full() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return ipcbuf_get_nfull(db);
}

auto ska::pst::smrb::DataBlock::get_header_bufs_available() -> uint64_t
{
  check_connected(true);
  return (header_nbufs - ipcbuf_get_nfull(&header_block));
}

auto ska::pst::smrb::DataBlock::get_data_bufs_available() -> uint64_t
{
  check_connected(true);
  auto db = reinterpret_cast<ipcbuf_t *>(&data_block);
  return (data_nbufs - ipcbuf_get_nfull(db));
}

auto ska::pst::smrb::DataBlock::get_data_read_xfer() -> uint64_t
{
  check_connected(true);
  return data_block.buf.sync->r_xfers[0];
}

auto ska::pst::smrb::DataBlock::get_data_write_xfer() -> uint64_t
{
  check_connected(true);
  return data_block.buf.sync->w_xfer;
}

auto ska::pst::smrb::DataBlock::get_writer_state() -> ska::pst::smrb::State
{
  check_connected(true);
  return static_cast<ska::pst::smrb::State>(data_block.buf.sync->w_state);
}

auto ska::pst::smrb::DataBlock::get_reader_state() -> ska::pst::smrb::State
{
  check_connected(true);
  return static_cast<ska::pst::smrb::State>(data_block.buf.sync->r_states[0]);
}
