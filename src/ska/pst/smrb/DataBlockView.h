/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <queue>

#include "ska/pst/smrb/DataBlockAccessor.h"

#ifndef __SKA_PST_SMRB_UTILS_DataBlockView_h
#define __SKA_PST_SMRB_UTILS_DataBlockView_h

namespace ska::pst::smrb
{

  /**
   * @brief Object Oriented interface to view to a PSRDADA Shared Memory Ring Buffer (SMRB).
   *
   */
  class DataBlockView : public DataBlockAccessor
  {
  public:
    /**
     * @brief Construct a new Data Block View object for the SMRB identified by the key
     *
     * @param key four-character hexidecimal key that identifies the SMRB in shared memory
     */
    explicit DataBlockView(const std::string &key);

    /**
     * @brief Destroy the Data Block View object.
     * Closes the access to the SMRB if required
     */
    ~DataBlockView();

    /**
     * @brief Open the SMRB for viewing
     *
     */
    void open() override;

    /**
     * @brief Clear the information last parsed from the header block
     * 
     * It is not necessary to close the SMRB when only viewing.
     *
     */
    void close() override;

    /**
     * @brief Effectively does nothing
     * 
     * Although viewers need not necessarily lock exclusive access to the ring buffer 
     * in shared memory, the assumptions built into DataBlockAccessor base class dictate
     * that the locked flag must be set; therefore, it is necessary to call this method
     * after calling open().
     *
     */
    void lock() override;

    /**
     * @brief Effectively does nothing (see note on lock() method)
     *
     */
    void unlock() override;

    /**
     * @brief Open the current viewing buffer in the data block
     *
     * @return pointer to the current viewing buffer in the data block
     */
    char *open_block() override;

    /**
     * @brief Internally flag the current data block buffer as viewed
     *
     * @param bytes effective ignored
     * @return ssize_t the number of bytes passed as an argument
     */
    ssize_t close_block(uint64_t bytes) override;

    /**
     * @brief Read the specified amount of data from the SMRB data block
     *
     * @param ptr pointer to buffer where the data will be stored
     * @param bytes size of buffer
     * @return ssize_t number of bytes read from the data block
     */
    ssize_t read_data(char *ptr, size_t bytes);

    /**
     * @brief Read the scan configuration from the header_block
     *
     */
    void read_config();

    /**
     * @brief Read the full header from the header_block
     *
     */
    void read_header();

    /**
     * @brief Go to the end of the SMRB data block
     *
     * @param byte_resolution how large of a chunck of data to allow for.
     * @return int Return 0 on success, -1 on failure
     */
    int seek_to_end(uint64_t byte_resolution = 0);

    /**
     * @brief Move the view to a particular part of the data block.
     *
     * @param offset the offset to apply in bytes
     * @param whence from where to apply the offset
     *
     */
    ssize_t seek(int64_t offset, int whence);

    /**
     * @brief Get the current buffer of the SMRB data_block.
     *
     */
    char *get_curr_buf();

    /**
     * @brief Get the index of the current buffer of the SMRB data_block.
     *
     */
    size_t get_current_buffer_index() const;

    /**
     * @brief Go to the specified buffer of the SMRB data block
     *
     * @param buffer_index ring buffer index in the data block
     */
    void seek_to_buffer_index(size_t buffer_index);

  private:
    void read_header_buffer(std::vector<char> &local_buffer);
  };

} // namespace ska::pst::smrb

#endif // __SKA_PST_SMRB_UTILS_DataBlockView_h
