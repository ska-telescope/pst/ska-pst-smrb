/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <memory>
#include <grpc++/grpc++.h>
#include <gtest/gtest.h>
#include "ska/pst/common/lmc/LmcService.h"
#include "ska/pst/common/statemodel/StateModel.h"
#include "ska/pst/smrb/DataBlockManager.h"
#include "ska/pst/smrb/DataBlockWrite.h"
#include "ska/pst/smrb/DataBlockRead.h"
#include "ska/pst/smrb/lmc/SmrbLmcServiceHandler.h"

#ifndef SKA_PST_SMRB_TESTS_SmrbLmcServiceHandlerTest_h
#define SKA_PST_SMRB_TESTS_SmrbLmcServiceHandlerTest_h

namespace ska::pst::smrb::test {

/**
 * @brief Simple test to check integration via LmcService
 *
 * @details This will do a simple walk through the obs state lifecycle.
 *
 */
class SmrbLmcServiceIntegrationTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

        grpc::Status configure_beam(bool dry_run = false);
        grpc::Status configure_beam(const ska::pst::lmc::ConfigureBeamRequest &request);
        grpc::Status get_beam_configuration(ska::pst::lmc::GetBeamConfigurationResponse* response);
        grpc::Status deconfigure_beam();

        grpc::Status configure_scan(bool dry_run = false);
        grpc::Status configure_scan(const ska::pst::lmc::ConfigureScanRequest &request);
        grpc::Status deconfigure_scan();
        grpc::Status get_scan_configuration(ska::pst::lmc::GetScanConfigurationResponse* response);

        grpc::Status start_scan();
        grpc::Status stop_scan();

        grpc::Status abort();
        grpc::Status reset();
        grpc::Status restart();
        grpc::Status go_to_fault();
        grpc::Status set_log_level(ska::pst::lmc::LogLevel required_log_level);
        grpc::Status get_log_level(ska::pst::lmc::GetLogLevelResponse& response);

        grpc::Status get_state(ska::pst::lmc::GetStateResponse*);
        void assert_state(ska::pst::lmc::ObsState);
        void assert_manager_state(ska::pst::common::State);
        void assert_log_level(ska::pst::lmc::LogLevel);

    public:
        SmrbLmcServiceIntegrationTest();
        ~SmrbLmcServiceIntegrationTest() = default;

        int _port = 0;
        std::shared_ptr<ska::pst::common::LmcService> _service{nullptr};
        std::shared_ptr<ska::pst::smrb::SmrbLmcServiceHandler> _handler{nullptr};
        std::shared_ptr<ska::pst::smrb::DataBlockManager> _smrb{nullptr};
        std::shared_ptr<grpc::Channel> _channel{nullptr};
        std::shared_ptr<ska::pst::lmc::PstLmcService::Stub> _stub{nullptr};

        std::string data_key{"a000"};
        std::string weights_key{"a010"};
        uint64_t hb_nbufs{5};
        uint64_t hb_bufsz{8192};
        uint64_t db_nbufs{6};
        uint64_t db_bufsz{1048576};
        uint64_t wb_nbufs{7};
        uint64_t wb_bufsz{1024};
        unsigned num_readers{1};
        int device_id{-1};

        std::shared_ptr<ska::pst::smrb::DataBlockWrite> _data_writer{nullptr};
        std::shared_ptr<ska::pst::smrb::DataBlockRead> _data_reader{nullptr};
        std::shared_ptr<ska::pst::smrb::DataBlockWrite> _weights_writer{nullptr};
        std::shared_ptr<ska::pst::smrb::DataBlockRead> _weights_reader{nullptr};
};

} // ska::pst::smrb::test

#endif // SKA_PST_SMRB_TESTS_SmrbLmcServiceHandlerTest_h
