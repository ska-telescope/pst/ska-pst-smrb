/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstddef>
#include <string>

#include "ska/pst/smrb/DataBlock.h"

#ifndef __SKA_PST_SMRB_DataBlockAccessor_h
#define __SKA_PST_SMRB_DataBlockAccessor_h

namespace ska {
namespace pst {
namespace smrb {

  /**
   * @brief Abstract base class that provides an Object Oriented interface
   * to interact with an existing PSRDADA Shared Memory Ring Buffer (SMRB).
   * 
   */
  class DataBlockAccessor : public DataBlock
  {
    public:

      /**
       * @brief Construct a new DataBlock with specified key.
       * 
       * @param key_string 4 character hexidecimal key that identifies the SMRB in shared memory.
       */
      explicit DataBlockAccessor(const std::string &key_string);

      /**
       * @brief Destroy the Data Block object
       * 
       */
      virtual ~DataBlockAccessor() = default;

      /**
       * @brief Attempt to connect to the existing SMRB within the specified timeout.
       * 
       * @param timeout the timeout for SMRB connection in seconds
       */
      void connect(int timeout) override;

      /**
       * @brief Disconnect from the SMRB.
       * 
       */
      void disconnect() override;

      /**
       * @brief Obtain a exclusive lock on the SMRB (reader or writer) to prevent other clients from accessing.
       * 
       */
      virtual void lock() = 0;

      /**
       * @brief Release the exclusive lock on the SMRB.
       * 
       */
      virtual void unlock() = 0;

      /**
       * @brief Open the SMRB for access
       *
       */
      virtual void open() = 0;

      /**
       * @brief Close the SMRB for access
       * 
       */
      virtual void close() = 0;

      /**
       * @brief Open a DataBlock buffer in the SMRB
       *
       * @return pointer to the opened DataBlock buffer
       */
      virtual char * open_block() = 0;

      /**
       * @brief Close a DataBlock buffer in the SMRB
       *
       * @param bytes number of bytes transacted in the DataBlock buffer
       * @return ssize_t Return 0 on success, -1 on failure
       */
      virtual ssize_t close_block(uint64_t bytes) = 0;

      /**
       * @brief Return true if a DataBlock buffer is open
       * 
       * @return true 
       * @return false 
       */
      inline const bool is_block_open() { return block_open; };

      /**
       * @brief Return true if the DataBlock buffer is full of data
       * 
       * @return true 
       * @return false 
       */
      inline const bool is_block_full() { return (curr_buf_bytes == data_bufsz); };

      /**
       * @brief Return the DataBlock buffer index
       * 
       * @return const uint64_t current data block index
       */
      const uint64_t get_buf_id() { return curr_buf_id; };

      /**
       * @brief Return the current number of bytes in the opened block
       * 
       * @return const uint64_t number of bytes of data in the opened block
       */
      const uint64_t get_buf_bytes() { return curr_buf_bytes; };

      /**
       * @brief Return a pointer to a copy of the current HeaderBlock C-string
       * 
       * @return const char* pointer to HeaderBlock C-string
       */
      const char * get_config() { return reinterpret_cast<const char *>(&config[0]); };

      /**
       * @brief Return a pointer to a copy of the current HeaderBlock C-string
       * 
       * @return const char* pointer to HeaderBlock C-string
       */
      const char * get_header() { return reinterpret_cast<const char *>(&header[0]); };

      /**
       * @brief Return the CUDA device ID for the data block.
       * If the DataBlock buffers exist in CUDA memory, the device will be >=0 
       * @return int CUDA device on which the DataBlock buffers reside, -1 if in CPU memory
       */
      int get_device() { return device_id; };

      /**
       * @brief Return true if the data block access has been locked for exclusivity.
       * 
       * @return true 
       * @return false 
       */
      bool get_locked() { return locked; }

      /**
       * @brief Return true if the data block has been opened for I/O.
       * 
       * @return true 
       * @return false 
       */
      bool get_opened() { return opened; }

      /**
       * @brief Check if the have_config attribute matches expected, throw exception if not
       * 
       * @param expected the expected value of the have_config attribute
       */
      void check_have_config(bool expected);

      /**
       * @brief Check if the have_header attribute matches expected, throw exception if not
       * 
       * @param expected the expected value of the have_header attribute
       */
      void check_have_header(bool expected);

      /**
       * @brief Clear the header contents and set have_header = false
       * 
       */
      void clear_header();

      /**
       * @brief Clear the config contents and set have_config = false
       * 
       */
      void clear_config();

    protected:

      //! helper method that throws runtime error if not locked
      void check_locked(bool expected);

      //! helper method that throws runtime error if not opened
      void check_opened(bool expected);

      //! True if connection to PSRDADA SMRB is locked
      bool locked{false};

      //! True if the data block has been opened for reading or writing
      bool opened{false};

      //! True if a PSRDADA SMRB DataBlock is opened for DMA
      bool block_open{false};

      //! Pointer to the currently open data_block buffer
      char * curr_buf{nullptr};

      //! Index of the currently open data_block buffer
      uint64_t curr_buf_id{0};

      //! Number of bytes transacted on the currently open data_block buffer
      uint64_t curr_buf_bytes{0};

      //! copy of the current scan configuration from the header_block buffer
      std::vector<char> config;

      //! copy of the current header configuration header_block buffer
      std::vector<char> header;

      bool have_config{false};

      bool have_header{false};

    private:

  };

} // smrb
} // pst
} // ska

#endif // __SKA_PST_SMRB_DataBlockAccessor_h
