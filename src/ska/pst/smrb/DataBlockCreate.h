/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <queue>

#include "ska/pst/smrb/DataBlock.h"

#ifndef __SKA_PST_SMRB_DataBlockCreate_h
#define __SKA_PST_SMRB_DataBlockCreate_h

namespace ska {
namespace pst {
namespace smrb {

  /**
   * @brief Object Oriented class that provides an interface for allocation and destruction
   * of PSRDADA Shared Memory Ring Buffers (SMRB).
   * 
   */
  class DataBlockCreate : public DataBlock
  {
    public:

      /**
       * @brief Construct a new Data Block Create object.
       * Supports creating an SMRB with the specified key which  accesses a PSRDADA Shared Memory
       * Ring Buffer (SMRB)
       * 
       * @param key 4 character hexidecimal key that identifies the SMRB in shared memory.
       */
      explicit DataBlockCreate(const std::string &key);

      /**
       * @brief Destroy the Data Block Write object.
       * Destruction of the object will result in destruction of the SMRB if it has not already
       * been destroyed.
       */
      ~DataBlockCreate();

      /**
       * @brief Creates an SMRB identified by the key using the specififed configuration.
       * Supports allocation in CPU or GPU memory.
       *
       * @param hdr_nbufs Number of header_block elements
       * @param hdr_bufsz Size of each header_block element in bytes
       * @param data_nbufs Number of data_block elements
       * @param data_bufsz Size of each data_block element in bytes
       * @param num_readers Number of readers that must process data written to the ring buffer
       * @param device_id CUDA GPU device on which the data_block element memory created, -1 indicates CPU memory
       */
      void create(uint64_t hdr_nbufs=8, uint64_t hdr_bufsz=4096, uint64_t data_nbufs=4, uint64_t data_bufsz=524288, unsigned num_readers=1, int device_id=-1);

      /**
       * @brief Destroys the SMRB identified by the key
       *
       */
      void destroy();

    protected:

    private:

      //! flag for whether the data block has been created
      bool created{false};

  };

} // smrb
} // pst
} // ska

#endif //# __SKA_PST_SMRB_DataBlockCreate_h
