/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/common/statemodel/StateModelException.h"

#include "ska/pst/smrb/DataBlockCreate.h"
#include "ska/pst/smrb/DataBlockStats.h"
#include "ska/pst/common/statemodel/ApplicationManager.h"


#ifndef __SKA_PST_SMRB_DataBlockManager_h
#define __SKA_PST_SMRB_DataBlockManager_h

namespace ska {
namespace pst {
namespace smrb {

  /**
   * @brief Manager for a Shared Memory Ring Buffer supporting the gRPC API for the SMRB component
   *
   */
  class DataBlockManager : public ska::pst::common::ApplicationManager
  {

    public:
      /**
       * @brief Default Construct a new Data Block Manager object
       *
       */
      DataBlockManager() : ska::pst::common::ApplicationManager("smrb")
      {
        initialise();
      };

      /**
       * @brief Destroy the Data Block Manager object
       *
       */
      ~DataBlockManager() = default;

      /**
       * @brief Configure beam for the data and weights ring buffers described by
       * the configuration file
       *
       * @param config_file configuration file describing the data and weights ring buffers
       */
      void configure_beam_from_file(const std::string &config_file);

      /**
       * @brief Print the data and weights ring buffer statistics
       *
       */
      void print_statistics();

      /**
       * @brief Return the statistics of the header_block of the data ring buffer
       *
       * @return DataBlockStats::stats_t header_block statistics of the data ring buffer
       */
      DataBlockStats::stats_t get_data_header_stats();

      /**
       * @brief Return the statistics of the data_block of the data ring buffer
       *
       * @return DataBlockStats::stats_t data_block statistics of the data ring buffer
       */
      DataBlockStats::stats_t get_data_data_stats();

      /**
       * @brief Return the statistics of the header_block of the weights ring buffer
       *
       * @return DataBlockStats::stats_t header_block statistics of the weights ring buffer
       */
      DataBlockStats::stats_t get_weights_header_stats();

      /**
       * @brief Return the statistics of the data_block of the weights ring buffer
       *
       * @return DataBlockStats::stats_t data_block statistics of the weights ring buffer
       */
      DataBlockStats::stats_t get_weights_data_stats();

      /**
       * @brief Check the configured flag matches the required value, throwing an exception if not
       *
       * @param required required value for the configured flag
       */
      void check_beam_configured(bool required);

      /**
       * @brief Return the value of the configured flag
       *
       * @return true the manager has been configured with data and weights keys
       * @return false the manage has not been configured with data and weights keys
       */
      bool is_beam_configured() { return beam_configured; };

      /**
       * @brief Wait until the specified duration has elapsed, or the signal is set to true
       *
       * @param milliseconds duration to wait for before returning
       * @param signal return immediately if the signal is true
       */
      void wait_for_duration(int milliseconds, volatile bool * signal);

      /**
       * @brief Wait until a signal observation has been processed by data DataBlock, or the signal set to true
       *
       * @param signal return immediately if the signal is true
       */
      void wait_for_observation(volatile bool * signal);

      /**
       * @brief Wait until the signal is set to boolean true
       *
       * @param signal return immediately if the signal is true
       */
      void wait_for_signal(volatile bool * signal);

      /**
       * @brief Get the ring buffer key of the currently managed ring buffers.
       *
       * @param header_key string value defined in std::vector<std::string> header_keys
       *
       * Calls to this should be preceeded with calls to
       * #is_beam_configured
       *
       * @return std::string of configured resources.
       * @throw std::runtime_exception if there are no currently beam configuration.
       */
      std::string get_beam_configuration_header_key(const std::string &header_key);

      /**
       * @brief Get the ring buffer size of the currently managed ring buffers.
       *
       * @param buffer_key string value defined in std::vector<std::string> buffer_keys
       *
       * Calls to this should be preceeded with calls to
       * #is_beam_configured
       *
       * @return std::string of configured resources.
       * @throw std::runtime_exception if there are no currently beam configuration.
       */
      uint64_t get_beam_configuration_buffer_key(const std::string &buffer_key);

      /**
       * @brief Validates Beam configuration.
       *
       * Validation errors should not be raise as exceptions but added to the
       * validation context.  The client of the method can decide what to do with
       * the validation errors.
       *
       * @param config Beam configuration to validate
       * @param context A validation context where errors should be added.
       */
      void validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Validates Scan configuration.
       *
       * Validation errors should not be raise as exceptions but added to the
       * validation context.  The client of the method can decide what to do with
       * the validation errors.
       *
       * @param config Scan configuration to validate
       * @param context A validation context where errors should be added.
       */
      void validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Validate requested start scan configuration
       *
       * @param config AsciiHeader containing Start Scan Configuration
       *
       */
      void validate_start_scan(const ska::pst::common::AsciiHeader& config);

      /**
       * @brief Initialisation callback.
       *
       */
      void perform_initialise();

      /**
       * @brief Beam Configuration callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from Idle to BeamConfigured.
       *
       */
      void perform_configure_beam();

      /**
       * @brief Scan Configuration callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from BeamConfigured to ScanConfigured.
       *
       */
      void perform_configure_scan();

      /**
       * @brief StartScan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from ScanConfigured to Scanning.
       * Launches perform_scan on a separate thread.
       *
       */
      void perform_start_scan();

      /**
       * @brief Scan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains scanning instructions meant to be launched in a separate thread.
       *
       */
      void perform_scan();

      /**
       * @brief StopScan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from Scanning to ScanConfigured.
       *
       */
      void perform_stop_scan();

      /**
       * @brief DeconfigureScan callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from ScanConfigured to BeamConfigured.
       *
       */
      void perform_deconfigure_scan();

      /**
       * @brief DeconfigureBeam callback that is called by \ref ska::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from BeamConfigured to Idle.
       *
       */
      void perform_deconfigure_beam();

      /**
       * @brief Terminate callback that is called by \ref ka::pst::common::ApplicationManager::main.
       * Contains the instructions required prior to transitioning the state from RuntimeError to Idle.
       *
       */
      void perform_terminate();

    private:
      //! Validates existence of header keys. Throws all missing keys.
      void check_header_keys(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      //! Validates pattern of config. Throws all values with invalid patterns.
      void check_value_pattern_match(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      //! List of expected header ring buffer keys - data and weights
      const std::vector<std::string> header_keys = {"DATA_KEY", "WEIGHTS_KEY"};

      //! List of expected header ring buffer keys - nbufs and bufsz. Use get_uint64 in AsciiHeader
      const std::vector<std::string> buffer_keys =  {"HB_NBUFS", "HB_BUFSZ", "DB_NBUFS", "DB_BUFSZ", "WB_NBUFS", "WB_BUFSZ"};

      //! Ring buffer key valid pattern
      const std::string key_pattern = "^[a-f\\d]{4,}$";

      //! Buffer value valid pattern
      const std::string buffer_pattern = "^[\\d]{1,}$";

      //! number of reader semaphores to create in the ring buffers
      const unsigned num_readers{1};

      //! CUDA device ID on which to create the ring buffers, -1 indicates in host memory
      const int device_id{-1};

      //! flag for where the ring buffers have been configured with their keys
      bool beam_configured{false};

      //! shared pointer to the data ring buffer under management
      std::shared_ptr<DataBlockCreate> data_ring_buffer;

      //! shared pointer to the weights ring buffer under management
      std::shared_ptr<DataBlockCreate> weights_ring_buffer;

      //! shared pointer to the statistics view of the data ring buffer
      std::shared_ptr<DataBlockStats> data_ring_stats;

      //! shared pointer to the statistics view of the weights ring buffer
      std::shared_ptr<DataBlockStats> weights_ring_stats;

  };

} // smrb
} // pst
} // ska

#endif // __SKA_PST_SMRB_DataBlockManager_h
