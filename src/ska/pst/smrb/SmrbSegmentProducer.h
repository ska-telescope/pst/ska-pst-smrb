/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>

#include "ska/pst/common/utils/BlockSegmentProducer.h"
#include "ska/pst/smrb/SmrbBlockProducer.h"

#ifndef __SKA_PST_SMRB_SmrbSegmentProducer_h
#define __SKA_PST_SMRB_SmrbSegmentProducer_h

namespace ska::pst::smrb
{

  /**
   * @brief Class used for reading voltage data and weights from file.
   *
   */
  class SmrbSegmentProducer : public ska::pst::common::BlockSegmentProducer
  {
  public:
    /**
     * @brief Create instance of a SmrbSegmentProducer object.
     *
     * @param data_key 4 character hexidecimal key that identifies the data SMRB in shared memory.
     * @param weights_key 4 character hexidecimal key that identifies the weights SMRB in shared memory.
     */
    SmrbSegmentProducer(
        const std::string &data_key,
        const std::string &weights_key);

    /**
     * @brief Destroy the SmrbSegmentProducer object.
     *
     */
    ~SmrbSegmentProducer();

    /**
     * @brief Attempt to connect to the existing data and weights SMRBs within the specified timeout,
     * and read the beam configurations in the data and weights SMRBs.
     *
     * @param timeout the timeout for SMRB connection in seconds
     */
    void connect(int timeout);

    /**
     * @brief Open the data and weights SMRBs for viewing
     * and read the scan configurations in the data and weights SMRBs.
     *
     * Call this method at the start of perform_scan in an application
     *
     */
    void open();

    /**
     * @brief Close the shared memory ring buffers
     *
     */
    void close();

    /**
     * @brief Get the next Segment of data and weights, ensuring that they are synchronized
     *
     */
    ska::pst::common::SegmentProducer::Segment next_segment() override;

    /**
     * @brief Disconnect from the SMRB.
     *
     */
    void disconnect();

    /**
     * @brief Get the offset of the segement's current data_block, from the start of the scan, in bytes.
     *
     * @return size_t offset from the start of the scan in bytes.
     */
    size_t get_obs_offset() const { return smrb_data_block_producer->get_obs_offset(); }

  private:
    /*! The shared pointers to BlockProducer instances in the common::BlockSegmentProducer
      base class point to the following SmrbBlockProducer instances; these pointers are
      maintained here for convenience in various methods. */
    std::shared_ptr<SmrbBlockProducer> smrb_data_block_producer;
    std::shared_ptr<SmrbBlockProducer> smrb_weights_block_producer;
  };

} // namespace ska::pst::smrb

#endif // __SKA_PST_SMRB_SmrbSegmentProducer_h
