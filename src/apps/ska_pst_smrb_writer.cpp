/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <cmath>
#include <iostream>
#include <vector>
#include <random>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/DataBlockWrite.h"
#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/Logging.h"
#include "ska/pst/common/utils/Time.h"
#include "ska/pst/common/utils/Timer.h"

void usage();
void signal_handler(int signal_value);
static bool sigint_received = false; // NOLINT

static constexpr unsigned default_blocks_to_write = 2;

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  spdlog::set_level(spdlog::level::info);
  int verbose = 0;

  // total number of data blocks to write
  unsigned blocks_to_write = default_blocks_to_write;

  // data rate at which to write data blocks
  double data_rate_MB_per_sec = 0;

  // zero memory copy flag
  bool zero_copy = false;

  // generate random data flag
  bool random = false;

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "b:chr:tvz")) != EOF)
  {
    switch(c)
    {
      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'b':
        blocks_to_write = atoi(optarg);
        break;

      case 'c':
        random = true;
        break;

      case 'r':
        data_rate_MB_per_sec = atof(optarg);
        break;

      case 'v':
        verbose++;
        break;

      case 'z':
        zero_copy = true;
        break;

      default:
        std::cerr << "Unrecognised option [" << c << "]" << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  int nargs = argc - optind;
  if (nargs != 3)
  {
    SPDLOG_ERROR("ERROR: 3 command line arguments expected");
    usage();
    return EXIT_FAILURE;
  }

  std::string config_file = std::string(argv[optind + 0]); // NOLINT
  std::string data_header_file = std::string(argv[optind + 1]); // NOLINT
  std::string weights_header_file = std::string(argv[optind + 2]); // NOLINT
  SPDLOG_DEBUG("config_file={} data_header={} weights_header={}", config_file, data_header_file, weights_header_file);

  signal(SIGINT, signal_handler);
  try
  {
    ska::pst::common::AsciiHeader beam_config, data_scan_config, weights_scan_config, data_header, weights_header;
    beam_config.load_from_file(config_file);
    data_scan_config.load_from_file(data_header_file);
    data_header.load_from_file(data_header_file);
    weights_scan_config.load_from_file(weights_header_file);
    weights_header.load_from_file(weights_header_file);

    // ensure the UTC_START and SCAN_ID are not present in the scan configuration
    data_scan_config.del("UTC_START");
    weights_scan_config.del("UTC_START");
    data_scan_config.del("SCAN_ID");
    weights_scan_config.del("SCAN_ID");

    double data_bytes_per_second = data_header.compute_bytes_per_second();

    std::string data_key = beam_config.get_val(std::string("DATA_KEY"));
    std::string weights_key = beam_config.get_val(std::string("WEIGHTS_KEY"));

    SPDLOG_INFO("creating writers for data and weights ring buffer");
    ska::pst::smrb::DataBlockWrite db(data_key);
    ska::pst::smrb::DataBlockWrite wb(weights_key);

    db.connect(0);
    wb.connect(0);

    db.lock();
    wb.lock();

    db.write_config(data_scan_config.raw());
    wb.write_config(weights_scan_config.raw());

    db.write_header(data_header.raw());
    wb.write_header(weights_header.raw());

    db.open();
    wb.open();

    uint64_t data_bufsz = db.get_data_bufsz();
    uint64_t weights_bufsz = wb.get_data_bufsz();

    static constexpr unsigned bytes_per_megabyte = 1048576;
    static constexpr unsigned microseconds_per_second = 1000000;

    // compute the delay between writing each block of data
    double block_period_seconds = 0;
    if (data_rate_MB_per_sec == 0)
    {
      block_period_seconds = static_cast<double>(data_bufsz) / data_bytes_per_second;
    }
    else if (data_rate_MB_per_sec > 0)
    {
      block_period_seconds = (static_cast<double>(data_bufsz) / data_rate_MB_per_sec * bytes_per_megabyte);
    }
    double block_period_microseconds = block_period_seconds * microseconds_per_second;
    SPDLOG_DEBUG("delay between blocks={} microseconds", block_period_microseconds);

    std::vector<char> data(data_bufsz, 0);
    std::vector<char> weights(weights_bufsz, 0);

    if (random)
    {
      std::string utc_start_str = data_header.get_val("UTC_START");
      ska::pst::common::Time utc_start(utc_start_str.c_str());
      time_t seed = utc_start.get_time();

      // create Standard mersenne_twister_engine seeded with
      std::mt19937 gen(seed);
      static constexpr int integers_per_char = 256;
      std::uniform_int_distribution<unsigned int> distrib(0, integers_per_char);

      for (unsigned i=0; i<data_bufsz; i++)
      {
        data[i] = static_cast<char>(distrib(gen));
      }
      for (unsigned i=0; i<weights_bufsz; i++)
      {
        weights[i] = static_cast<char>(distrib(gen));
      }
    }

    uint64_t total_bytes = (data_bufsz + weights_bufsz) * blocks_to_write;
    SPDLOG_DEBUG("writing {} bytes of data in {} blocks", total_bytes, blocks_to_write);

    ska::pst::common::Timer timer;
    for (unsigned i=0; i<blocks_to_write; i++)
    {
      char * dptr = db.open_block();
      char * wptr = wb.open_block();

      if (!zero_copy)
      {
        memcpy(dptr, &data[0], data_bufsz);
        memcpy(wptr, &weights[0], weights_bufsz);
      }

      db.close_block(data_bufsz);
      wb.close_block(weights_bufsz);

      SPDLOG_DEBUG("adding delay of {} microseconds", block_period_microseconds);
      timer.wait_until(block_period_microseconds);
    }

    timer.print_rates(total_bytes);

    db.close();
    wb.close();

    db.unlock();
    wb.unlock();

    db.disconnect();
    wb.disconnect();
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return -1;
  }

  return 0;
}

void usage()
{
  std::cout << "Usage: ska_pst_smrb_writer config data_header weights_header\n"
    "Writes zero's to the data and weights shared memory ring buffers in accordance with the data/weights headers\n"
    "Quit with CTRL+C.\n"
    "\n"
    "  config           configuration file describing the ring buffers\n"
    "  data_header      ascii header describing the data time series\n"
    "  weights_header   ascii header describing the weights time series\n"
    "  -h               print this help text\n"
    "  -c               write random data sequence, seeded by UTC_START into ring buffers\n"
    "  -b               number of blocks of data to write [default " << default_blocks_to_write << "]\n"
    "  -r rate          override the natural data rate in MB/s, value of -1 indicates no limit\n"
    "  -z               do not simulate memory writing into the data blocks\n"
    "  -v               verbose output\n"
    << std::endl;
}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (sigint_received)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  sigint_received = true;
}
