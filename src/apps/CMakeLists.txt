
add_executable (ska_pst_smrb_core ska_pst_smrb_core.cpp)
add_executable (ska_pst_smrb_info ska_pst_smrb_info.cpp)
add_executable (ska_pst_smrb_writer ska_pst_smrb_writer.cpp)
add_executable (ska_pst_smrb_reader ska_pst_smrb_reader.cpp)

target_link_libraries (ska_pst_smrb_core ska_pst_smrb)
target_link_libraries (ska_pst_smrb_info ska_pst_smrb)
target_link_libraries (ska_pst_smrb_writer ska_pst_smrb)
target_link_libraries (ska_pst_smrb_reader ska_pst_smrb)

install (
  TARGETS
    ska_pst_smrb_core
    ska_pst_smrb_info
    ska_pst_smrb_writer
    ska_pst_smrb_reader
  DESTINATION
    bin
)

add_test(NAME ska_pst_smrb_core COMMAND ska_pst_smrb_core -f "${CMAKE_CURRENT_LIST_DIR}/data/config.txt" -t)
add_test(NAME ska_pst_smrb_info COMMAND ska_pst_smrb_info)
add_test(NAME ska_pst_smrb_writer COMMAND ska_pst_smrb_writer -h)
add_test(NAME ska_pst_smrb_reader COMMAND ska_pst_smrb_reader -h)
