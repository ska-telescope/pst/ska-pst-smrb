/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <iostream>
#include <vector>
#include <spdlog/spdlog.h>
#include <thread>

#include "ska/pst/smrb/config.h"
#include "ska/pst/smrb/DataBlockManager.h"
#include "ska/pst/smrb/DataBlockStats.h"
#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/Logging.h"

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
#include "ska/pst/common/lmc/LmcService.h"
#include "ska/pst/smrb/lmc/SmrbLmcServiceHandler.h"
#endif

void usage();
void signal_handler(int signal_value);
static bool signal_received = false; // NOLINT

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  spdlog::set_level(spdlog::level::info);
  static constexpr int test_duration_ms = 100;

  std::string config_file{};

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  int control_port = -1;
  std::string service_name = "SMRB";
#endif

  int verbose = 0;

  int duration = -1;

  bool single_observation = false;

  opterr = 0;

  int c = 0;
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  while ((c = getopt(argc, argv, "c:f:n:hstv")) != EOF)
#else
  while ((c = getopt(argc, argv, "f:hstv")) != EOF)
#endif
  {
    switch(c)
    {
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
      case 'c':
        control_port = atoi(optarg);
        break;
      case 'n':
        service_name = std::string(optarg);
        break;
#endif

      case 'f':
        config_file = std::string(optarg);
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 's':
        single_observation = true;
        break;

      case 't':
        duration = test_duration_ms;
        break;

      case 'v':
        verbose++;
        break;

      default:
        std::cerr << "Unrecognised option [" << c << "]" << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  SPDLOG_DEBUG("config_file={}", config_file);
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  SPDLOG_DEBUG("control_port={}, service_name={}", control_port, service_name);
#endif
  // Check arguments
  int nargs = argc - optind;
  if (nargs != 0)
  {
    SPDLOG_ERROR("ERROR: 0 command line arguments expected");
    usage();
    return EXIT_FAILURE;
  }

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  if (config_file.length() == 0 && control_port == -1)
  {
    SPDLOG_ERROR("ERROR: require either a configuration file or control port");
#else
  if (config_file.length() == 0)
  {
    SPDLOG_ERROR("ERROR: require a configuration file");
#endif
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  std::shared_ptr<ska::pst::common::LmcService> grpc_control = nullptr;
#endif

  try
  {
    std::shared_ptr<ska::pst::smrb::DataBlockManager> smrb = std::make_shared<ska::pst::smrb::DataBlockManager>();

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
    if (control_port != -1)
    {
      SPDLOG_INFO("Setting up gRPC contoller on port {}", control_port);
      auto lmc_handler = std::make_shared<ska::pst::smrb::SmrbLmcServiceHandler>(smrb);
      grpc_control = std::make_shared<ska::pst::common::LmcService>(service_name, lmc_handler, control_port);
      grpc_control->start();
    }
    else
#endif
    {
      smrb->configure_beam_from_file(config_file);
    }

    SPDLOG_DEBUG("waiting for termination condition");

    // wait for an exit condition
    if (duration > 0)
    {
      smrb->wait_for_duration(duration, &signal_received);
    }
    else if (single_observation)
    {
      smrb->wait_for_observation(&signal_received);
    }
    else
    {
      smrb->wait_for_signal(&signal_received);
    }

#ifdef HAVE_PROTOBUF_gRPC_LMCPB
    if (grpc_control)
    {
      // signal the gRPC server to exit
      SPDLOG_INFO("Stopping gRPC controller");
      grpc_control->stop();
      SPDLOG_TRACE("gRPC contoller has stopped");
    }
#endif
    smrb->quit();
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return -1;
  }

  return 0;
}

void usage()
{
  std::cout << "Usage: ska_pst_smrb_core [options]" << std::endl;
  std::cout <<   "Create shared memory ring buffers from the configuration file or via control command." << std::endl;
  std::cout <<   "Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  std::cout << "  -c port     port on which to accept control commands" << std::endl;
  std::cout << "  -n name     name of the service" << std::endl;
#endif
  std::cout << "  -f config   ascii file containing observation configuration" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -s          single observation, destroy the data blocks once a single observation has been processed" << std::endl;
  std::cout << "  -t          test mode, destroy the data blocks shortly after creating themn" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (signal_received)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  signal_received = true;
}
